<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 20/05/2017
 * Time: 22:17
 */

use Elasticsearch\Common\Exceptions\ElasticsearchException;
use MundiEstudo\Application;
use MundiEstudo\core\config\IniConfig;
use MundiEstudo\core\persister\ElasticSearchPersister;
use MundiEstudo\model\Person\Person;

require_once "vendor/autoload.php";


/*
$config = new IniConfig();
$config->load('database.ini');

$persister = new ElasticSearchPersister($config);



$book = new \MundiEstudo\model\Item\BookItem();
$book->setName("Como enriquecer vendendo Latinhas");
$book->setTags(array());
$book->setWriter("Jão Candango");
$book->setRating(4.3);
$book->setSynopsis(
    "Mestre da bricolagem e exímio catador de latinhas,  Jão Candango ensina " .
    "como sobrever à crise econômica se virando e revirando. De latinhas de cerveja a " .
    "latas de óleo antigas, passando por tonéis (também latinhas, se você for um gigante), " .
    "Jão mostra diferentes técnicas de pesagem e valorização das principais variedades de ".
    "'cascas de enlatados', como ele costuma chamar. Leitura obrigatória."
);
$persister->create($book);

$book = new \MundiEstudo\model\Item\BookItem();
$book->setName("Harry Potter e o Prisioneiro das Quebradas");
$book->setTags(array());
$persister->create($book);

$dvd = new \MundiEstudo\model\Item\DVDItem();
$dvd->setName("Tempos de Zoeira");
$dvd->setTags(array());
$persister->create($dvd);


$dvd = new \MundiEstudo\model\Item\DVDItem();
$dvd->setName("Era uma vez em Pindamonhangaba do Sul");
$dvd->setTags(array());
$persister->create($dvd);

$dvd = new \MundiEstudo\model\Item\DVDItem();
$dvd->setName("Homem de Lata");
$dvd->setTags(array());
$persister->create($dvd);

$cd = new \MundiEstudo\model\Item\CDItem();
$cd->setName("Estaca 7");
$cd->setTags(array());
$persister->create($cd);

$cd = new \MundiEstudo\model\Item\CDItem();
$cd->setName("Inarmonia do Funk");
$cd->setTags(array());
$persister->create($cd);

$cd = new \MundiEstudo\model\Item\CDItem();
$cd->setName("Heavy Pagode");
$cd->setTags(array());
$persister->create($cd);

$cd = new \MundiEstudo\model\Item\CDItem();
$cd->setName("Cantigas de Ninar para Cavalos");
$cd->setTags(array());
$persister->create($cd);

die();*/





try {
    $app = new Application();
    $app->run();
}
catch(Exception $exception){
    die("ok");
}