#Gerenciador de Coleções

Este pequeno projeto consiste no desenvolvimento de um gerenciador de coleções.
Através dele, você pode gerenciar sua coleção e Livros, CDs e DVDs. O sistema mantém
um registro das pessoas que podem ou não estar com itens da coleção emprestados. 
 
##Recursos

####Cadastro de Itens
O sistema permite que Livros, CDs e DVDs sejam cadastrados e editados, 
e podem ser adicionadas Tags a estes itens. 
Os tem apenas o campo nome em comum.  

######Livros
O autor e a sinópse do livro podem ser cadastrados.
######CD
O artista e as faixas do CD podem ser cadastrados.
######DVD
O diretor e a sinópse do DVD podem ser cadastrados.

####Cadastro de Pessoas
Pessoas, juntamente com seu contato, podem ser cadastradas e editadas no sistema;

####Empréstimo de items
Items cadastrados no sistema podem ser emprestados a pessoas, e o sistema mantém 
o registro de quem está com cada item, e se o item está disponível ou emprestado.

####Busca de itens
É possível buscar por uma palavra chave e ter como resultado qualquer elemento
 contendo aquela palavra em qualquer um de seus campos. 
 Os items buscáveis são: Livro, CD, DVD, Pessoa e Tag.
 
####Filtro de itens
Na página principal do sistema é possivel filtrar os itens que serão exibidos, através das 
checkboxes correspondentes.

##Tecnologias utilizadas

- PHP 7.1 (requerido, pois é usado o recurso Nullable Types)
- Slim Framework 3
- ElasticSearch 5.4.0
- AngularJS 1.6.4
- PHPUnit (testes ainda sendo escritos)
- Bower
- Composer

Para outras tecnologias/pacotes utilizados, consulte os arquivos composer.json e bower.json.




