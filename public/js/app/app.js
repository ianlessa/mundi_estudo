/**
 *  Este arquivo define o módulo principal AngularJS da aplicação.
 *  @author Ian Lessa
 */
angular.module("mundi_estudo",[
    'ui.router', /* Módulo para roteamento */
    'ngMaterial', /* Módulo para widgets */
    'ngRateIt', /*Módulo para widget de classificação usando estrelas */


    'mundi_estudoRoutes' /*Módulo de configuração de rotas de views */
]);
var app = angular.module("mundi_estudo");