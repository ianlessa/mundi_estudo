/**
 * Serviço de fábrica de objetos. Retorna um objeto com as propriedades inicializadas, de acordo com o tipo passado
 * para seu método create.
 *
 * @author Ian Lessa
 */

app.service('ObjectFactory',function(){

    var self = this;


    /**
     * Método fábrica de objetos. Cria um item de acordo com o tipo passado em seu parâmetro itemType.
     * Os parâmetros que serão inicializados respeitam os parâmetros dos
     * objetos defindos no backend, na pasta app/model.
     *
     * Esta método basicamente instancia os objetos para que eles sejam compatíveis com o método
     * app/traits/FrontendTranslatorTrait::import(), no backend.
     *
     * @author Ian Lessa
     * @param itemType tipo do item a serem criados,
     * @returns {*} O objeto inicializado.
     */
    self.create = function(itemType){

        switch(itemType) { //de acordo com o tipo do item, retorna a estrutura do objeto inicializada.

            case 'book' : return {
                type:'item/book',
                name: '',
                writer: '',
                synopsis: '',
                rating: 0,
                tags: []
            };
            case 'cd' : return {
                type: 'item/cd',
                name: '',
                artist: '',
                tracks: [],
                rating: 0,
                tags: []
            };
            case 'dvd' : return {
                type: 'item/dvd',
                name: '',
                director: '',
                synopsis: '',
                rating: 0,
                tags: []
            };
            case 'person' : return {
                type:'persons/person',
                name: '',
                contact: ''
            };
        }
    }

});