/**
 * Esta Fábrica define um interceptador de HTTP, que interceptará as requisições ajax à API,
 * para que eventos sejam disparados no inicio e fim das requisições, e evenntos específicos sejam disparados
 * dependendo do estado das respostas HTTP.
 *
 * @author Ian Lessa
 */

app.factory('httpRequestInterceptor', function ($q,$rootScope) {
    return {

        'request': function(config) { //interceptando o inicio da requisição
            $rootScope.$broadcast('requestStart'); //disparando evento requestStart
            return config;
        },

        'response': function(response) { //interceptando o fim da requisição
            $rootScope.$broadcast('requestEnd'); //disparando evento
            return response;
        },

        'responseError': function(rejection) { //interceptando estado de erro.
            switch(rejection.status) { //pegando o estado da resposta
                case 401: //Unauthorized
                    $rootScope.$broadcast('$routeChangeError', rejection); //disparando evento.
                    break;
                case 500: //Internal Server Error
                    $rootScope.$broadcast('$serverError', rejection); //disparando evento
                    break;
                default:
                    if(rejection.data === null) { //se não houve resposta
                        rejection.data = "Erro na comunicação com o servidor.";
                    }
                    console.log("Http Interceptor Response error:" +  rejection.status); //loga no console o estado ...
                    $rootScope.$broadcast('$serverError', rejection); //...e dispara o evento
            }
            return $q.defer(); //continua a requisição.
        }
    };
});



