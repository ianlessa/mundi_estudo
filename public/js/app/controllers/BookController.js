app.controller("BookController",function($scope,books,ItemService,TagService,DialogService,ObjectFactory,PersonService) {
    $scope.books = books.data;

    $scope.ItemService = ItemService;
    $scope.TagService = TagService;


    $scope.itemCreationDialog = function() {
        var item = ObjectFactory.create('book');
        DialogService.itemEditDialog(item,"Novo Livro",function(newBook){
            ItemService.create(newBook,function(createdBook){
                $scope.books.unshift(createdBook);
            });
        });
    };

    $scope.itemEditDialog = function(item) {

        var originalBook = {};
        angular.copy(item,originalBook);

        DialogService.itemEditDialog(item,"Editar Livro",function(editedBook){
            ItemService.update(editedBook);
        },function () {
            angular.copy(originalBook,item);
        });

    };


    $scope.deletionConfirm = function(item) {
        var content = "O livro '" + item.name +"' será apagado. Deseja continuar?";
        DialogService.confirmDialog('Deseja apagar este livro?',content,function(){
            ItemService.delete(item,function(){
                $scope.books = $scope.books.filter(function(element){
                   return element.id !== item.id;
                });
            });
        });
    }


    $scope.manageBorrower = function(item) {
        if(ItemService.isBorrowed(item)) {
            item.borrowerId = '';
            ItemService.update(item);
            return;
        }

        PersonService.load(function(){
            DialogService.borrowerDialog(item,function(){
                ItemService.update(item);
            });
        });

    }

});