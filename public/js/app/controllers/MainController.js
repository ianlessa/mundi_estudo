app.controller("MainController",function($scope,$state,$mdDialog,TagService,PersonService,ItemService, DialogService){
    "use strict";

    //definindo comportamento visual de troca de item ativo no menu.
    $("#mainNav li").click(function(event) {
        $("#mainNav li").removeClass("active");
        $(event.currentTarget).addClass("active");
    });


    //event listeners e barra de progresso principal.
    $scope.isMainLoading = false; //estado da barra de progresso de carregamento

    $scope.$on("requestStart", function () {$scope.isMainLoading = true;});
    $scope.$on("requestEnd", function () {$scope.isMainLoading = false;});
    $scope.$on("$viewContentLoaded", function () {$scope.isLoading = false;});
    $scope.$on("$routeChangeError", function () {$scope.isLoading = false;});
    $scope.$on("$serverError", function (event,data) {

        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Ops, ocorreu um erro!')
                .textContent(data.data)
                .ariaLabel('Ops, ocorreu um erro!')
                .ok('OK :(')
        );

        $scope.isLoading = false;
    });


    TagService.load();
    PersonService.load();


    $scope.mainSearchText = '';
    $scope.search = function() {
        ItemService.search($scope.mainSearchText,function($results){
            DialogService.searchResultsDialog($results);
        });
    };




    //redirecionando para rota principal da aplicação
    $state.go('itemIndex',{} ,{reload: true});

});