app.controller("CDController",function($scope,cds,ItemService,TagService,PersonService,DialogService,ObjectFactory) {
    $scope.cds = cds.data;
    $scope.ItemService = ItemService;
    $scope.TagService = TagService;

    $scope.itemCreationDialog = function() {
        var item = ObjectFactory.create('cd');
        DialogService.itemEditDialog(item,"Novo CD",function(newItem){
            ItemService.create(newItem,function(createdItem){
                $scope.cds.unshift(createdItem);
            });
        });
    };

    $scope.deletionConfirm = function(item) {
        var content = "O CD '" + item.name +"' será apagado. Deseja continuar?";
        DialogService.confirmDialog('Deseja apagar este CD?',content,function(){
            ItemService.delete(item,function(){
                $scope.cds = $scope.cds.filter(function(element){
                    return element.id !== item.id;
                });
            });
        });
    };

    $scope.itemEditDialog = function(item) {
        var originalItem = {};
        angular.copy(item,originalItem);
        DialogService.itemEditDialog(item,"Editar CD",function(editedItem){
            ItemService.update(editedItem);
        },function () {
            angular.copy(originalItem,item);
        });
    };

    $scope.manageBorrower = function(item) {
        if(ItemService.isBorrowed(item)) {
            item.borrowerId = '';
            ItemService.update(item);
            return;
        }

        PersonService.load(function(){
            DialogService.borrowerDialog(item,function(){
                ItemService.update(item);
            });
        });

    }
});