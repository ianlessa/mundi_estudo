app.controller("PersonController",function($scope,persons,ObjectFactory,DialogService,ItemService) {

    $scope.persons = persons.data;
    $scope.ItemService = ItemService;

    $scope.personCreationDialog = function() {
        var item = ObjectFactory.create('person');
        DialogService.itemEditDialog(item,"Nova pessoa",function(newItem){
            ItemService.create(newItem,function(createdItem){
                $scope.persons.unshift(createdItem);
            });
        });
    };

    $scope.deletionConfirm = function(item) {
        var content = "A Pessoa '" + item.name +"' será apagada. Deseja continuar?";
        DialogService.confirmDialog('Deseja apagar esta pessoa?',content,function(){
            ItemService.delete(item,function(){
                $scope.persons = $scope.persons.filter(function(element){
                    return element.id !== item.id;
                });
            });
        });
    };

    $scope.itemEditDialog = function(item) {

        var originalItem = {};
        angular.copy(item,originalItem);

        DialogService.itemEditDialog(item,"Editar Pessoa",function(editedItem){
            ItemService.update(editedItem);
        },function () {
            angular.copy(originalItem,item);
        });

    };

});