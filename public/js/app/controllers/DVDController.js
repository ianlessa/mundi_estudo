app.controller("DVDController",function($scope,dvds,ItemService,TagService,ObjectFactory,DialogService,PersonService) {
    $scope.dvds = dvds.data;
    $scope.ItemService = ItemService;
    $scope.TagService = TagService;

    $scope.itemCreationDialog = function() {
        var item = ObjectFactory.create('dvd');
        DialogService.itemEditDialog(item,"Novo DVD",function(newItem){
            ItemService.create(newItem,function(createdItem){
                $scope.dvds.unshift(createdItem);
            });
        });
    };

    $scope.deletionConfirm = function(item) {
        var content = "O DVD '" + item.name +"' será apagado. Deseja continuar?";
        DialogService.confirmDialog('Deseja apagar este DVD?',content,function(){
            ItemService.delete(item,function(){
                $scope.dvds = $scope.dvds.filter(function(element){
                    return element.id !== item.id;
                });
            });
        });
    };

    $scope.itemEditDialog = function(item) {
        var originalItem = {};
        angular.copy(item,originalItem);

        DialogService.itemEditDialog(item,"Editar DVD",function(editedItem){
            ItemService.update(editedItem);
        },function () {
            angular.copy(originalItem,item);
        });

    };


    $scope.manageBorrower = function(item) {
        if(ItemService.isBorrowed(item)) {
            item.borrowerId = '';
            ItemService.update(item);
            return;
        }

        PersonService.load(function(){
            DialogService.borrowerDialog(item,function(){
                ItemService.update(item);
            });
        });

    }

});