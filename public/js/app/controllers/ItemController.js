app.controller("ItemController",function($scope,items,ItemService,TagService,DialogService,PersonService) {
    $scope.items = items.data;
    $scope.ItemService = ItemService;
    $scope.TagService = TagService;

    $scope.showItem = {
        book: true,
        cd: true,
        dvd: true,
        borrowed: true,
        notBorrowed: true
    };

    $scope.manageBorrower = function(item) {
        if(ItemService.isBorrowed(item)) {
           item.borrowerId = '';
           ItemService.update(item);
           return;
        }

        PersonService.load(function(){
            DialogService.borrowerDialog(item,function(){
                ItemService.update(item);
            });
        });

    };

    var applyWatch = false;
    $scope.$watch('showItem', function() {
        if(applyWatch) {
            ItemService.getAll(function(items){
                var filtered = items;
                if(!$scope.showItem.borrowed) {
                    filtered = filtered.filter(function(item){
                        return item.borrowerId === '';
                    });
                }
                if(!$scope.showItem.notBorrowed) {
                    filtered = filtered.filter(function(item){
                        return item.borrowerId !== '';
                    });
                }
                if(!$scope.showItem.book) {
                    filtered = filtered.filter(function(item){
                       return item.type !== 'item/book';
                    });
                }
                if(!$scope.showItem.cd) {
                    filtered = filtered.filter(function(item){
                        return item.type !== 'item/cd';
                    });
                }
                if(!$scope.showItem.dvd) {
                    filtered = filtered.filter(function(item){
                        return item.type !== 'item/dvd';
                    });
                }
                $scope.items = filtered;
            });
        }
        applyWatch = true;
    },true);

});