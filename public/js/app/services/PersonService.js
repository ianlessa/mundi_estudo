/**
 * Serviço de acesso à API RESTFul, para operações com pessoas
 *
 * @author Ian Lessa
 */
app.service('PersonService',function($http){
    var self = this;
    self.persons = []; //para armazenar a lsita de pessoas

    /**
     * Recupera uma lista de pessoas da API e a armazena em self.persons.
     *
     * @param callback Função que será chamada após a requisição ajax ser completada.
     */
    self.load = function(callback) {
        $http.get('item/person').then(function(data){ //requisitando a API
            self.persons = data.data;
            if(typeof callback !== 'undefined') { //se callback estiver definida, a chama.
                callback();
            }
        });
    };

    /**
     * Retorna a lista de pessoas atualmente carregada
     *
     * @author Ian Lessa
     * @returns {Array|*} A lista de pessoas
     */
    self.getPersons = function() {
        return self.persons;
    };

    /**
     * Retorna uma pessoa cujo id seja personId.
     *
     * @param personId Id da pessoa a ser procurada.
     * @returns {*|{}} A pessoa a encontrada ou undefined, se não for encontrada uma pessoa com aquele id.
     */
    self.getById = function(personId) {
        return self.persons.find(function(element){
            return element.id === personId;
        });
    }
});