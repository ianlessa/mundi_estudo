/**
 * Serviço de acesso à API RESTFul, para operações com livros, cds e dvds.
 * Também contém métodos helpers
 *
 * @author Ian Lessa
 */
app.service('ItemService',function($http,PersonService){
    var self = this;

    /**
     * Retorna a classe Font Awesome correta para cada tipo de item.
     *
     * @author Ian Lessa
     * @param item O item cujo tipo será consultado.
     * @returns {*} A classe Font Awesome.
     */
    self.getTypeIcon = function(item) {
        switch(item.type) {
            case "item/book": return 'fa fa-book';
            case "item/cd": return 'fa fa-music';
            case "item/dvd": return 'fa fa-film';
            case "persons/person": return 'fa fa-user';
            case "tags/tag": return 'fa fa-tag';
            default: return 'fa fa-question';
        }
    };

    /**
     * Converte o tipo do item para seu nome em linguagem natural.
     *
     * @author Ian Lessa
     * @param item O item cujo tipo será consultado
     * @returns {*} O nome do item
     */
    self.getTypeName = function(item) {
        switch(item.type) {
            case "item/book": return 'Livro';
            case "item/cd": return 'CD';
            case "item/dvd": return 'DVD';
            case "persons/person": return 'Pessoa';
            case "tags/tag": return 'TAG';
            default: return 'Genérico';
        }
    };

    /**
     * Retorna a pessoa com quem o item está emprestado.
     *
     * @author Ian Lessa
     * @param item O item
     * @returns {*|{}|*} A pessoa
     */
    self.getBorrower = function(item) {
        /*consulta, na lista de pessoas existentes e carregadas,
            qual pessoa corresponde àquele id e a retorna.
         */
        return PersonService.getById(item.borrowerId);
    };

    /**
     * Verifica se o item está atualmente emprestado.
     *
     * @author Ian Lessa
     * @param item O Item a ser verificado.
     * @returns {boolean} True se está emprestado, false se não
     */
    self.isBorrowed = function(item) {
        //se o borrowerId estiver vazio, o item não está emprestado.
        return item.borrowerId.length > 0;
    };

    /**
     *
     * @deprecated Será removido em breve.
     * @author Ian Lessa
     * @param person
     * @param item
     */
    self.borrow = function(person,item) {
        console.log(person.name +' borrow ' + item.name);
    };
    /**
     * @deprecated Será removido em breve.
     * @author Ian Lessa
     * @param item
     */
    self.edit = function(item) {
        console.log('edit ' + item.name);
    };

    /** Atualiza um item, fazendo uma requisição PATCH à API RESTFul.
     *
     * @author Ian Lessa
     * @param item O item, já contendo as informações que serão atualizadas.
     */
    self.update = function(item) {
        var type = self.getItemType(item); //para que a url da requisição possa ser definida, pega antes o tipo do item.
        $http.patch('item/' + type,item); //requista a atualização à API, enviando o item a ser atualizado.
    };


    /**
     * Deleta um item, fazendo uma requisição DELETE à API RESTFul.
     *
     * @author Ian Lessa
     * @param item O Item que será apagado
     * @param callback A função que será chamada após a requisição ser completada.
     */
    self.delete = function(item,callback) {
        //para que a url da requisição possa ser definida, pega antes o tipo do item.
        var type = self.getItemType(item);
        /*
            requisita a deleção à api. O id do item é passado na url também,
            devido a incompatibiliade dos navegadores com o método DELETE.
         */
        $http.delete('item/' + type + "/" + item.id ,item).then(function(){
            if(typeof callback !== 'undefined') { //após a deleção, se callback for passada, ela é chamada.
                callback();
            }
        },function(){});
    };

    /**
     * Solicita a criação de um item à API RESTFul, através do método POST.
     *
     * @author Ian Lessa
     * @param item O item que será criado.
     * @param callback Função que será chamada após a requisição à API ser completada.
     */
    self.create = function(item,callback) {
        //para que a url da requisição possa ser definida, pega antes o tipo do item.
        var type = self.getItemType(item);
        //Requisita a API a criação do item, passando a url e o item a ser criado.
        $http.post('item/' + type,item).then(function(data){
            if(typeof callback !== 'undefined') { //se calback for passada, a chama.
                callback(data.data);
            }
        },function(){});
    };

    /**
     * Atualiza a classificação do item.
     * Como O AngularJS Dispõe de two-way data binding, no momento que o item for passado para este método
     * sua classificação já vai ser a nova definda pelo usuário. Portanto, basta apenas uma atualização do item.
     *
     * @author Ian Lessa
     * @param item O item cuja classificação será atualizada.
     */
    self.rate = function(item) {
        self.update(item); //atualiza o item com a nova classificação.
    };

    /**
     * Retorna o tipo do item, baseado em seu atributo type. Este atributo respeita as mesmas regras do atributo
     * da classe de model do backend correspondente.
     *
     * @author Ian Lessa
     * @param item O item cujo atributo type sera utilizado.
     * @returns {*} o tipo do item.
     */
    self.getItemType = function (item) {
        return item.type.split("/")[1]; //separa o tipo correto do item.
    };

    /**
     * Requisita à API RESTful, utilizando o método GET,  a lista com todos os cds, cds e dvds cadastrados.
     *
     * @author Ian Lessa
     * @param callback A função que será chamada após a requisição ser completada.
     * Seu único parâmetro receberá a lista dos itens encontrados.
     */
    self.getAll = function (callback){
        //requisitando a lista à API
        $http({method: 'GET', url: 'item'}).then(function(data){
                callback(data.data); //chamando a callback, e passando a lista recebida como parâmetro.
            },function(){
                callback([]);
            }
        );
    };

    /**
     * Realiza uma pesquisa na API RESTFul. Os resultados incluirão Livros, CDs, DVDs, Pessoas e Tags.
     * Todos os campos destes objetos serão pesquisados.
     *
     * @author Ian Lessa
     * @param term O tempo que será pesquisado em todos os campos dos objetos cadastrados.
     * @param callback A função que será chamada após a requisição ser completada.
     * Seu único parâmetro receberá a lista dos itens encontrados.
     */
    self.search = function (term,callback){
        $http.get('itemSearch/' + term).then(
            function(data) {
                callback(data.data); //chamando a callback, e passando os resultados da pesquisa como parâmetro.
            },
            function(){
                callback([]); //em caso da requisição retornar erro, uma array vazia é passada para callback.
            }
        )
    }
});