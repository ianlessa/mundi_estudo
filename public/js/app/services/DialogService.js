/**
 * Este serviço é responsável pela criação dos diversos diálogos do sistema.
 *
 * @author Ian Lessa
 */
app.service('DialogService',function($mdDialog,ItemService,PersonService){

    var self = this;

    /**
     * Abre o diálogo de criação/edição de item.
     * O template do dialogo aberto será definido pelo tipo de item que será criado/editado
     *
     * @author Ian Lessa
     * @param item O item que será criado/editado
     * @param dialogTitle O título do diálogo
     * @param okCallback A função que será chamada quando o diálogo for confirmado/salvo
     * @param cancelCallback A função que será chamada quando o diálogo for fechado/cancelado.
     */
    self.itemEditDialog = function (item,dialogTitle,okCallback,cancelCallback){
        //pegando o tipo do item para definir a url do template do diálogo.
        var itemType = ItemService.getItemType(item);
        //usuando o módulo AngularJS Material para exibir o diálogo.
        $mdDialog.show({
            /*função controladora que será usada no dialogo.*/
            controller: function($scope,$mdDialog,TagService) {
                $scope.TagService = TagService; //deixando disponível o serviço TagService no escopo do diálogo.
                $scope.dialogTitle = dialogTitle;
                $scope.item = item; //deixando o item disponível no escopo do diálogo.
                $scope.cancel = function() { //definindo a função cancelar.
                    $mdDialog.cancel();
                    if(typeof cancelCallback !== 'undefined') { //se cancelCallback for definida, será chamda.
                        cancelCallback(item);
                    }
                };
                //definindo a função save do dialogo.
                $scope.save = function(item) {
                    $mdDialog.hide(item);
                };
            },
            /*A url de template/view do dialogo.*/
            templateUrl: '_view/'+itemType+'.partials.editForm',
            /* Definindo que é possível fechar o diálogo clicando fora dele.*/
            clickOutsideToClose:true
        }).then(function(item) { //quando o dialogo for confirmado.
                if(typeof okCallback !== 'undefined') { //se okCallback for definida, será chamada.
                    okCallback(item);
                }
        },function(item){ //quando o diálogo é cancelado/fechado
            if(typeof cancelCallback !== 'undefined') {  //se cancelCallback for definda, será chamada.
                cancelCallback(item);
            }
        });
    };

    /**
     * Abre um diálogo de confirmação simples.
     *
     * @author Ian Lessa
     * @param title O título do diálogo
     * @param content O conteúdo textual do diálogo
     * @param callback A função que será chamada caso o diálogo seja confirmado.
     */
    self.confirmDialog = function(title,content,callback) {
        //configura o diálogo.
        var confirm = $mdDialog.confirm()
            .title(title)
            .textContent(content)
            .ariaLabel('Confirmar')
            .ok('Sim')
            .cancel('Não');
        //exibe o diálogo configurado...
        $mdDialog.show(confirm).then(function() {
            callback(); //..e chama sua callback,quando ele for confirmado.
        }, function() { //se o diálogo for fechado sem confirmar, não faz nada.
        });
    };

    /**
     * Exibe o diálogo de empréstimo de item.
     *
     * @param item O item que será emprestado
     * @param callback A função que será chamada após o empréstimo ser confirmado.
     */
    self.borrowerDialog = function (item,callback){
        $mdDialog.show({
            /*A função controladora do diálogo. */
            controller: function($scope,$mdDialog) {
                $scope.dialogTitle = 'Emprestar Item';
                $scope.item = item;
                $scope.persons = PersonService.getPersons();//pegando a lista de pessoas que podem pegar itens
                //definindo a pessoa selecionada no select.
                $scope.selectedBorrowerId = $scope.persons.length > 0 ? $scope.persons[0].id : '';
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.save = function(item) {
                    //atualizando quem está com o item.
                    item.borrowerId = $scope.selectedBorrowerId;
                    $mdDialog.hide(item);
                };
            },
            /* a url da View do diálogo. */
            templateUrl: '_view/item.partials.borrowerDialog',
            /* Definindo que é possível fechar o diálogo clicando fora dele.*/
            clickOutsideToClose:true
        }).then(function(item) {
            if(typeof callback !== 'undefined') { //a callback que será chamada após a confirmação do empréstimo.
                callback(item);
            }
        },function(){
        });
    };

    /**
     * Exíbe um díalogo contendo os resultados de uma busca.
     *
     * @author Ian Lessa
     * @param $results A lista de resultados da busca.
     */
    self.searchResultsDialog = function($results){
        $mdDialog.show({
            /*A função controladora do diálogo. */
            controller: function($scope,$mdDialog) {
                $scope.results = $results;
                $scope.ItemService = ItemService;
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
            },
            /* A url da view do diálogo. */
            templateUrl: '_view/main.partials.searchResultsDialog',
            /* Definindo que é possível fechar o diálogo clicando fora dele.*/
            clickOutsideToClose:true
        }).then(function() {
            if(typeof callback !== 'undefined') {  //a callback que será chamada após a confirmação do empréstimo.
                callback(item);
            }
        },function(){
        });
    }




});