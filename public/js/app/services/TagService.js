/**
 * Serviço para controle e acesso à API para Tags.
 *
 * @author Ian Lessa
 */
app.service('TagService',function($http,ItemService){

    var self = this;
    var tags = []; //armazenará a lista de tags carregadas.

    /**
     * Requisita à API RESTFull a lista de tags cadastradas, utilizando o método GET.
     *
     * @author Ian Lessa
     * @param callback A função que será chamada após a requisição ser completada.
     */
    self.load = function(callback) {
        //requisitando a lista de Tags
        $http.get('item/tag').then(function(data){
            tags = data.data; //armazenando o retorno da requisição na variavel local do serviço.
            if(typeof callback !== 'undefined') { //se callback estiver definida, a chama.
                callback();
            }
        });
    };

    /**
     * Solicita à API RESTFul a criação de uma nova tag, através do método POST. Após a criação, adiciona a nova
     * tag à lista de tags carregadas.
     *
     * @author Ian Lessa
     * @param tagName O nome da nova tag que será criada.
     * @param callback A função que será chamada após a requisição ser completada.
     */
    self.create = function(tagName,callback){
        //requisitando a criação, através do método POST, e passando um objeto simples contendo o nome da tag
        $http.post('item/tag',{name: tagName}).then(function(data){
            //adicionando o retorno da requisição (a nova tag criada) à lista.
            var newTag = data.data;
            tags.push(newTag);
            if(typeof callback !== 'undefined') { //se callback for passada, é chamada.
                callback(newTag);
            }
        });
    };

    /**
     * Retorna uma tag pelo seu Id
     *
     * @author Ian Lessa
     * @param tagId o id da tag que será procurada
     * @returns {*|{}} Se a tag for encontrada na lista de tags carregadas é retornada. Se não, o retorno é undefined.
     */
    self.getById = function (tagId) {
        return tags.find(function(element){
            return element.id === tagId;
        });
    };

    /**
     * Callback que será chamada pelo widget md-autocomplete do módulo AngularJS Material.
     * Esta callback será usada em conjunto com o widget md-chips, do mesmo módulo.
     *
     * Pesquisa, na lista de tags carregadas, alguma que contenha o termo passado em query.
     *
     * @author Ian Lessa
     * @param query O termo a ser procurado.
     * @returns {*} Uma array contendo os resultados da busca.
     */
    self.search = function(query) {
        var lowerQuery = query.toLowerCase(); //convertendo o termo para letras minúsculas.
        return query ? tags.filter(function(tag){
            //comparando o termo com o nome da tag, também em minúsculo.
            return tag.name.toLowerCase().indexOf(lowerQuery) !== -1;
        }) : [];
    };

    /**
     * Callback que será chamada pelo widget md-chips do módulo AngularJS Material, para retornar o objeto correto
     * a ser adicionado em seu input.
     * Este método também atualiza o item passado como parâmetro com a nova tag inserida, além de cadastrar novas tags
     * se estas ainda não existirem.
     *
     * @author Ian Lessa
     * @param tag O objeto ou nome de Tag
     * @param item O item no qual a tag será adicionada.
     * @param updateItem se este parâmetro for diferente de false, atualiza o item com a nova tag.
     * @returns {*} o id da tag criada/encontrada.
     */
    self.transformTag = function(tag,item,updateItem){
        if(angular.isObject(tag)) { //se tag for um objeto, significa que ele já foi cadastrado.
            //se a tag ainda não estiver definida no item
            if(typeof item.tags.find(function(element){
                return element === tag.id
            }) === 'undefined') {
                item.tags.push(tag.id); //adicionando a tag ao item
            }
            //se updateItem for qualquer coisa diferente de false
            if(!(typeof updateItem !== 'undefined' && updateItem===false)) {
                ItemService.update(item); //atualiza o item
            }
            return tag.id; //retorna o id da tag.
        }
        //se não for objeto
        var result = self.search(tag); //procura pela tag na lista de carregadas
        if(result.length == 1) { //se for encontrado um resultado.
            //se a tag ainda não estiver inserida no item.
            if(typeof item.tags.find(function(element){
                    return element === result[0].id;
                }) === 'undefined') {
                item.tags.push(result[0].id); //adiciona a tag
            }
            //se updateItem for qualquer coisa diferente de false
            if(!(typeof updateItem !== 'undefined' && updateItem===false)) {
                ItemService.update(item); //atualiza o item
            }
            return result[0].id; //retorna o id da tag.
        }
        else { //se a tag não for encontrada na lista de tags carregadas
            /*
                adicionando um placeholder, para que seja exibida a animação de loading enquanto a tag é criada
                e, após, possa ser substituída pela tag real que for criada.
             */
            var placeHolder = tag + item.tags.length;
            //requisitando a criação da tag à api.
            self.create(tag,
                /*callback que será chamada após a criação da tag, o parametro newTag receberá a tag criada.*/
                function(newTag) {
                    //recuperando o índice na lista de tags do item onde o placeholder foi colocado.
                    var placeHolderIndex = item.tags.indexOf(placeHolder);
                    item.tags[placeHolderIndex] = newTag.id; //substituindo o placeholder pelo id da nova tag criada.
                    //se updateItem for qualquer coisa diferente de false
                    if(!(typeof updateItem !== 'undefined' && updateItem===false)) {
                        ItemService.update(item); //atualiza o item
                    };
                }
            );
        }
        return tag + item.tags.length; //se chegar neste retorno, é pq a tag está sendo criada pela api.
    };

    /**
     * Retorna a lista de tags carregadas.
     *
     * @author Ian Lessa
     * @returns {Array}
     */
    self.getTags = function () {
        return tags;
    }
});