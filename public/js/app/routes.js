/**
 * Este arquivo define o módulo AngularJS que configura as rotas para as views do frontend.
 *
 * @author Ian Lessa
 */
angular.module("mundi_estudoRoutes",[]).config(function($httpProvider, $stateProvider){

    /*
        interceptador de requisições http, para disparar o eventos quando
        houverem requisições ajax.
     */
    $httpProvider.interceptors.push('httpRequestInterceptor');

    /**
     * Esta é a configuração de uma rota para uma view.
     *
     * @author Ian Lessa
     */
    $stateProvider
        .state('itemIndex', { /*o estado relacionado à view */
            templateUrl: "_view/item.index", /*A url do template da view*/
            controller : "ItemController", /*O controlador AngularJS que será associado à view*/
            resolve: { /*Antes de instanciar o controlador da view, as dependências serão resolvidas*/
                /* Para limpar o cache da view, e sempre ter as informações mais atuais */
                clearTemplateCache: function($templateCache){
                    $templateCache.remove('_view/item.index');
                },
                /* para resolver a dependência que será injetada no controlador da view.*/
                items: function($http){
                    return $http({method: 'GET', url: 'item'}); //requisitando a lista de itens à API RESTFul
                }
            }
        })
    ;

    /**
     * Mais uma configuração de view. Para detalhes sobre os atributos, veja a configuração
     * da view itemIndex, definida neste arquivo.
     *
     * @author Ian Lessa
     */
    $stateProvider
        .state('bookIndex', {
            templateUrl: "_view/book.index",
            controller : "BookController",
            resolve: {
                clearTemplateCache: function($templateCache){
                    $templateCache.remove('_view/book.index');
                },
                books: function($http){
                    return $http({method: 'GET', url: 'item/book'}); /* Recuperando a lista de livros da api */
                }
            }
        })
    ;

    /**
     * Mais uma configuração de view. Para detalhes sobre os atributos, veja a configuração
     * da view itemIndex, definida neste arquivo.
     *
     * @author Ian Lessa
     */
    $stateProvider
        .state('cdIndex', {
            templateUrl: "_view/cd.index",
            controller : "CDController",
            resolve: {
                clearTemplateCache: function($templateCache){
                    $templateCache.remove('_view/cd.index');
                },
                cds: function($http){
                    return $http({method: 'GET', url: 'item/cd'}); /*Recuperando a lista de cds da api*/
                }
            }
        })
    ;

     /**
     * Mais uma configuração de view. Para detalhes sobre os atributos, veja a configuração
     * da view itemIndex, definida neste arquivo.
     *
     * @author Ian Lessa
     */
    $stateProvider
        .state('dvdIndex', {
            templateUrl: "_view/dvd.index",
            controller : "DVDController",
            resolve: {
                clearTemplateCache: function($templateCache){
                    $templateCache.remove('_view/dvd.index');
                },
                dvds: function($http){
                    return $http({method: 'GET', url: 'item/dvd'}); /*Recuperando a lista de dvds da api*/
                }
            }
        })
    ;

    /**
     * Mais uma configuração de view. Para detalhes sobre os atributos, veja a configuração
     * da view itemIndex, definida neste arquivo.
     *
     * @author Ian Lessa
     */
    $stateProvider
        .state('personIndex', {
            templateUrl: "_view/person.index",
            controller : "PersonController",
            resolve: {
                clearTemplateCache: function ($templateCache) {
                    $templateCache.remove('_view/person.index');
                },
                persons: function ($http) {
                    return $http({method: 'GET', url: 'item/person'}); /*Recuperando a lista de pessoas da api*/
                }
            }
        })
    ;


});