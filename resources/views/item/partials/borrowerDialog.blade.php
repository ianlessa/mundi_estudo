<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 28/05/2017
 * Time: 12:50
 */
?>


<md-dialog aria-label="dialogTitle">
    <form ng-cloak>
        <md-toolbar>
            <div class="md-toolbar-tools">
                <h2>Emprestar '@{{ item.name }}'</h2>
                <span flex></span>
                <md-button class="md-icon-button" aria-label = 'Fechar Diálogo' ng-click="cancel()">
                    <i class="fa fa-close"></i>
                </md-button>
            </div>
        </md-toolbar>

        <md-dialog-content>
            <label>Emprestar para:</label>
            <md-input-container>
                <md-select aria-label="Emprestar Para" ng-model="selectedBorrowerId">
                    <md-option ng-value="person.id" ng-repeat="person in persons">
                        @{{person.name}}
                    </md-option>
                </md-select>
            </md-input-container>

        </md-dialog-content>

        <md-dialog-actions layout="row">
            <md-button ng-click="cancel()">
                Cancelar
            </md-button>
            <span flex></span>
            <md-button ng-click="save(item)" class="md-raised">
                Emprestar
            </md-button>
        </md-dialog-actions>
    </form>
</md-dialog>

