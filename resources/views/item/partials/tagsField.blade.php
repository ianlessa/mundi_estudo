<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 27/05/2017
 * Time: 10:21
 */
?>

<md-chips ng-model="item.tags" md-autocomplete-snap
md-transform-chip="TagService.transformTag($chip,item)"
md-require-match="false"
md-on-remove="ItemService.update(item)"
>
<md-autocomplete
        md-no-cache="true"
        md-selected-item="selectedItem"
        md-search-text="searchText"
        md-items="tag in TagService.search(searchText)"
        md-item-text="tag.name"
        placeholder="Insira uma Tag">
    <span md-highlight-text="searchText">@{{tag.name}}</span>
</md-autocomplete>
<md-chip-template>
        <span>
          <strong ng-if="TagService.getById($chip).name">
              @{{  TagService.getById($chip).name }}
          </strong>
          <i ng-if="!TagService.getById($chip).name" class='fa fa-spinner faa-spin animated'></i>
        </span>
</md-chip-template>
</md-chips>