<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 17:03
 */
?>
<div layout="row" layout-align="center center" layout-wrap>
    <div>
        <md-checkbox ng-model="showItem.book" aria-label="Mostrar Livros" >
            <md-tooltip md-direction="top">Mostrar Livros</md-tooltip>
            <i class="fa fa-book"></i>
        </md-checkbox>
        <md-checkbox ng-model="showItem.cd" aria-label="Mostrar Livros" >
            <md-tooltip md-direction="top">Mostrar CDs</md-tooltip>
            <i class="fa fa-music"></i>
        </md-checkbox>
        <md-checkbox ng-model="showItem.dvd" aria-label="Mostrar Livros" >
            <md-tooltip md-direction="top">Mostrar DVDs</md-tooltip>
            <i class="fa fa-film"></i>
        </md-checkbox>
        <md-checkbox ng-model="showItem.borrowed" aria-label="Mostrar Livros" >
            <md-tooltip md-direction="top">Itens Emprestados</md-tooltip>
            Items Emprestados
        </md-checkbox>
        <md-checkbox ng-model="showItem.notBorrowed" aria-label="Mostrar Livros" >
            <md-tooltip md-direction="top">Itens não emprestados</md-tooltip>
            Itens não emprestados
        </md-checkbox>
    </div>
</div>

<md-card md-theme="default" ng-repeat="item in items">
    <md-card-title>
        <md-card-title-text>
           <span class="md-headline">
               <i class="@{{ ItemService.getTypeIcon(item) }}"></i>
               @{{item.name}}
               <ng-rate-it ng-model="item.rating" ng-click="ItemService.rate(item)">
           </span>
        </md-card-title-text>
    </md-card-title>
    <md-card-content>
        @include('item.partials.tagsField')
    </md-card-content>
    <md-card-actions layout="row" layout-align="space-between center">
        <span><div  ng-if="ItemService.getBorrower(item).name">
            Emprestado com: @{{ ItemService.getBorrower(item).name }} (
            @{{ ItemService.getBorrower(item).contact }})
        </div></span>
        <md-button ng-click="manageBorrower(item)" class="md-raised">
            <i class = 'fa fa-user'></i>
            <i class = "fa
              fa-@{{ ItemService.isBorrowed(item) ? 'arrow-right ' : 'arrow-left'}}">
            </i>
            @{{ ItemService.isBorrowed(item) ? ' Pegar de volta' : ' Emprestar' }}
        </md-button>
    </md-card-actions>
</md-card>