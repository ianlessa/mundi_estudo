<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 17:03
 */
?>
<md-button ng-click="itemCreationDialog()" class="md-raised">
    Novo DVD
</md-button>
<md-card md-theme="default" ng-repeat="item in dvds">
    <md-card-title>
        <md-card-title-text>
           <span class="md-headline">
               @{{item.name}}
               <small class="md-subhead" ng-if="item.director.length > 0"> - @{{item.director}}</small>
               <ng-rate-it ng-model="item.rating" ng-click="ItemService.rate(item)">
           </span>
        </md-card-title-text>
    </md-card-title>
    <md-card-content>
        @include('item.partials.tagsField')
        <div ng-if="item.synopsis.length > 0">
            <h4>Sinópse</h4>
            <p>@{{ item.synopsis }}</p>
        </div>
    </md-card-content>
    <md-card-actions layout="row" layout-align="space-between center">
        <div>
            <md-button ng-click="itemEditDialog(item)" class="md-raised">
                <i class = 'fa fa-pencil'></i> Editar
            </md-button>
            <md-button ng-click="deletionConfirm(item)" class="md-raised">
                <i class = 'fa fa-trash'></i> Apagar
            </md-button>
        </div>
        <span><div  ng-if="ItemService.getBorrower(item).name">
            Emprestado com: @{{ ItemService.getBorrower(item).name }} (
            @{{ ItemService.getBorrower(item).contact }})
        </div></span>
        <md-button ng-click="manageBorrower(item)" class="md-raised">
            <i class = 'fa fa-user'></i>
            <i class = "fa
              fa-@{{ ItemService.isBorrowed(item) ? 'arrow-right ' : 'arrow-left'}}">
            </i>
            @{{ ItemService.isBorrowed(item) ? ' Pegar de volta' : ' Emprestar' }}
        </md-button>
    </md-card-actions>
</md-card>