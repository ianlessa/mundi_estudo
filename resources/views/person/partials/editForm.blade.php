<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 27/05/2017
 * Time: 22:04
 */
?>

<md-dialog aria-label="dialogTitle">
    <form ng-cloak>
        <md-toolbar>
            <div class="md-toolbar-tools">
                <h2>@{{ dialogTitle }}</h2>
                <span flex></span>
                <md-button class="md-icon-button" aria-label = 'Fechar Diálogo' ng-click="cancel()">
                    <i class="fa fa-close"></i>
                </md-button>
            </div>
        </md-toolbar>

        <md-dialog-content>
            <br />
            <div layout="row" layout-wrap>
                <md-input-container flex="50">
                    <label>Nome</label>
                    <input ng-model="item.name">
                </md-input-container>
                <md-input-container flex="50">
                    <label>Contato</label>
                    <input ng-model="item.contact">
                </md-input-container>
                <br />
            </div>
        </md-dialog-content>

        <md-dialog-actions layout="row">
            <md-button ng-click="cancel()" class="md-raised">
                Cancelar
            </md-button>
            <span flex></span>
            <md-button ng-click="save(item)" class="md-raised">
                Salvar
            </md-button>
        </md-dialog-actions>
    </form>
</md-dialog>
