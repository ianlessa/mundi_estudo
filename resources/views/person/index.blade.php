<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 17:02
 */
?>
<md-button ng-click="personCreationDialog()" class="md-raised">
    Nova Pessoa
</md-button>
<div layout="row" layout-wrap ng-repeat="person in persons">
    <md-input-container flex="40">
        <label>Nome</label>
        <input ng-model="person.name" disabled>
    </md-input-container>
    <md-input-container flex="30">
        <label>Contato</label>
        <input ng-model="person.contact" disabled>
    </md-input-container>
    <md-button ng-click="itemEditDialog(person)" class="md-raised">
        <i class = 'fa fa-pencil'></i> Editar
    </md-button>
    <md-button ng-click="deletionConfirm(person)" aria-label="apagar pessoa" class="md-raised">
        <i class = 'fa fa-trash'></i> Apagar
    </md-button>
    <hr />
</div>