<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 08:37
 */
?>
<ul class="nav nav-pills" id="mainNav">
    <li id="itensNavBt" class="active">
        <a ui-sref="itemIndex"><i class="fa fa-gift"></i> Todos os Itens</a>
    </li>
    <li id="booksNavBt">
        <a ui-sref="bookIndex"><i class="fa fa-book"></i> Livros</a>
    </li>
    <li id="cdsNavBt">
        <a ui-sref="cdIndex"><i class="fa fa-music"></i> CDs</a>
    </li>
    <li id="dvdsNavBt">
        <a ui-sref="dvdIndex"><i class="fa fa-film"></i> DVDs</a>
    </li>
    <li id="personsNavBt" style="float:right">
        <a ui-sref="personIndex"><i class="fa fa-users"></i> Pessoas</a>
    </li>
</ul>
