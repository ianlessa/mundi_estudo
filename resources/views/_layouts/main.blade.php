<!DOCTYPE html>
<html lang="en" ng-app="mundi_estudo">
<head>
    <meta charset="UTF-8">
    <title>Gerenciador de Coleções</title>
    <link rel="stylesheet" href="public/css/bootstrap.css" />
    <link rel="stylesheet" href="public/css/bootstrap-theme.css" />
    <link rel="stylesheet" href="public/css/angular-material.css" />
    <link rel="stylesheet" href="public/css/font-awesome.min.css" />
    <link rel="stylesheet" href="public/css/ng-rateit.css" />
    <link rel="stylesheet" href="public/css/font-awesome-animation.css" />

    <link rel="stylesheet" href="public/css/app.css" />

    <link rel="shortcut icon" href="public/images/logo-mundipagg.ico">

</head>
<body ng-controller="MainController" class="body">
    @yield('content')
</body>
<script type="text/javascript" src="public/js/angular.js"></script>
<script type="text/javascript" src="public/js/jquery.min.js"></script>
<script type="text/javascript" src="public/js/bootstrap.min.js"></script>

<script type="text/javascript" src="public/js/angular-ui-router.js"></script>
<script type="text/javascript" src="public/js/angular-aria.js"></script>
<script type="text/javascript" src="public/js/angular-animate.js"></script>
<script  type="text/javascript" src="public/js/angular-material.js"></script>
<script  type="text/javascript" src="public/js/ng-rateit.js"></script>

<script type="text/javascript" src="public/js/app/routes.js"></script>
<script type="text/javascript" src="public/js/app/app.js"></script>
<script type="text/javascript" src="public/js/app/httpInterceptor.js"></script>
<script type="text/javascript" src="public/js/app/factories/ObjectFactory.js"></script>
<script type="text/javascript" src="public/js/app/services/PersonService.js"></script>
<script type="text/javascript" src="public/js/app/services/TagService.js"></script>
<script type="text/javascript" src="public/js/app/services/ItemService.js"></script>
<script type="text/javascript" src="public/js/app/services/DialogService.js"></script>
<script type="text/javascript" src="public/js/app/controllers/MainController.js"></script>
<script type="text/javascript" src="public/js/app/controllers/ItemController.js"></script>
<script type="text/javascript" src="public/js/app/controllers/BookController.js"></script>
<script type="text/javascript" src="public/js/app/controllers/CDController.js"></script>
<script type="text/javascript" src="public/js/app/controllers/DVDController.js"></script>
<script type="text/javascript" src="public/js/app/controllers/PersonController.js"></script>



</html>