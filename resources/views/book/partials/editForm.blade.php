<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 27/05/2017
 * Time: 22:04
 */
?>

<md-dialog aria-label="dialogTitle">
    <form ng-cloak>
        <md-toolbar>
            <div class="md-toolbar-tools">
                <h2>@{{ dialogTitle }}</h2>
                <span flex></span>
                <md-button class="md-icon-button" aria-label = 'Fechar Diálogo' ng-click="cancel()">
                    <i class="fa fa-close"></i>
                </md-button>
            </div>
        </md-toolbar>

        <md-dialog-content>
            <br />
            <div layout="row" layout-wrap>
                <md-input-container flex="50">
                    <label>Nome</label>
                    <input ng-model="item.name">
                </md-input-container>
                <md-input-container flex="50">
                    <label>Autor</label>
                    <input ng-model="item.writer">
                </md-input-container>
                <br />
            </div>
            <div layout="row" layout-wrap>
                <md-input-container flex="100">
                    <label>Sinópse</label>
                    <textarea ng-model="item.synopsis"></textarea>
                </md-input-container>
            </div>
            <div layout="row" layout-wrap>
                <label>Classificação</label>
                <ng-rate-it ng-model="item.rating">
            </div>
            <div layout="row" layout-wrap>
                @include('item.partials.newItemTagsField')
            </div>

        </md-dialog-content>

        <md-dialog-actions layout="row">
            <md-button ng-click="cancel()" class="md-raised">
                Cancelar
            </md-button>
            <span flex></span>
            <md-button ng-click="save(item)" class="md-raised">
                Salvar
            </md-button>
        </md-dialog-actions>
    </form>
</md-dialog>
