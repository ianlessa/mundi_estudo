@extends('_layouts.main')
@section('content')
<div class="container-fluid"><div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <md-progress-linear class="pageLoadingProgressBar" ng-show="isMainLoading" md-mode="indeterminate"></md-progress-linear>
                <div class="page-header" layout="row" layout-wrap>
                    <h2>
                        Gerenciador de Coleções
                    </h2>
                    <span flex></span>
                    <div>
                        <md-input-container>
                            <label>Buscar</label>
                            <input ng-model="mainSearchText">
                        </md-input-container>
                        <md-button aria-label = 'Buscar' class="md-icon-button md-raised" ng-click="search()">
                            <md-tooltip md-direction="top">Buscar</md-tooltip>
                            <i class="fa fa-search"></i>
                        </md-button>
                    </div>
                </div>
                @include('_layouts.mainNavbar')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ui-view></ui-view>
            </div>
        </div>
    </div>
@endsection