<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 28/05/2017
 * Time: 18:06
 */
?>
<md-dialog aria-label="dialogTitle">
    <form ng-cloak>
        <md-toolbar>
            <div class="md-toolbar-tools">
                <h2>Resultados da busca</h2>
                <span flex></span>
                <md-button class="md-icon-button" aria-label = 'Fechar Diálogo' ng-click="cancel()">
                    <i class="fa fa-close"></i>
                </md-button>
            </div>
        </md-toolbar>
        <md-dialog-content>
            <p ng-if="results.length <= 0"> Sem Resultados</p>
            <table class="table table-striped" ng-if="results.length > 0">
                <thead>
                <tr>
                    <td><strong>Tipo</strong></td>
                    <td><strong>Nome</strong></td>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="result in results">
                    <td>
                        <md-tooltip md-direction="right">@{{ ItemService.getTypeName(result) }}</md-tooltip>
                        <i class="@{{ ItemService.getTypeIcon(result) }}"></i>
                    </td>
                    <td>
                        @{{ result.name }}
                    </td>
                </tr>
                </tbody>
            </table>
        </md-dialog-content>
        <md-dialog-actions layout="row">
            <md-button ng-click="cancel()" class="md-raised">
                OK
            </md-button>
        </md-dialog-actions>
    </form>
</md-dialog>