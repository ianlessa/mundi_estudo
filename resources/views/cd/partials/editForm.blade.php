<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 27/05/2017
 * Time: 22:04
 */
?>

<md-dialog aria-label="dialogTitle">
    <form ng-cloak>
        <md-toolbar>
            <div class="md-toolbar-tools">
                <h2>@{{ dialogTitle }}</h2>
                <span flex></span>
                <md-button class="md-icon-button" aria-label = 'Fechar Diálogo' ng-click="cancel()">
                    <i class="fa fa-close"></i>
                </md-button>
            </div>
        </md-toolbar>

        <md-dialog-content>
            <br />
            <div layout="row" layout-wrap>
                <md-input-container flex="50">
                    <label>Nome</label>
                    <input ng-model="item.name">
                </md-input-container>
                <md-input-container flex="50">
                    <label>Artista</label>
                    <input ng-model="item.artist">
                </md-input-container>
                <br />
            </div>
            <div layout="row" layout-wrap>
                <md-button ng-click="item.tracks.push('')" class="md-raised">
                    Adicionar Faixa
                </md-button>
            </div>
            <div layout="row" layout-wrap>
                <table ng-if="item.tracks.length > 0" flex="100" class="table table-striped">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td><strong>Faixa</strong></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="itemTrack in item.tracks track by $index">
                            <td>
                                @{{ $index + 1 }}
                            </td>
                            <td>
                                <md-input-container>
                                    <input aria-label="Faixa" ng-model="item.tracks[$index]">
                                </md-input-container>
                            </td>
                            <td>
                              <md-button aria-label="Apagar faixa" ng-click="item.tracks.splice($index,1)" class="md-icon-button">
                                  <i class="fa fa-trash"></i>
                              </md-button>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <div layout="row" layout-wrap>
                <label>Classificação</label>
                <ng-rate-it ng-model="item.rating">
            </div>
            <div layout="row" layout-wrap>
                @include('item.partials.newItemTagsField')
            </div>

        </md-dialog-content>

        <md-dialog-actions layout="row">
            <md-button ng-click="cancel()" class="md-raised">
                Cancelar
            </md-button>
            <span flex></span>
            <md-button ng-click="save(item)" class="md-raised">
                Salvar
            </md-button>
        </md-dialog-actions>
    </form>
</md-dialog>
