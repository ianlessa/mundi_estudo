<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 27/05/2017
 * Time: 09:52
 */

namespace MundiEstudo;


use Slim\App;

class Application
{
    /**
     * @var App O objeto de aplicação Slim.
     */
    private $slim;

    public function __construct()
    {

        $slimContainer = new \Slim\Container([
            'settings' => [
                'displayErrorDetails' => true,
            ],
            'errorHandler' => function ($slimContainer) {
                return function ($request, $response, $exception) use ($slimContainer) {
                    return $slimContainer['response']->withStatus(500)
                        ->withHeader('Content-Type', 'text/html')
                        ->write(
                            get_class($exception) . " : " .
                            $exception->getMessage()
                        );
                };
            }
        ]);

        $app = new App($slimContainer);
        require_once "routes.php";
        $this->slim = $app;

    }


    public function run() {
        $this->slim->run();
    }

}