<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 20/05/2017
 * Time: 23:30
 */

namespace MundiEstudo\core\view;

use Jenssegers\Blade\Blade;
use Psr\Http\Message\ResponseInterface as Response;


/**
 * Classe servidora de Views.
 * @package MundiEstudo\core\view
 * @author Ian Lessa
 */
class ViewServer
{


    /**
     * Serve uma view, formatada com a template engine Blade.
     * O arquivo da view deve estar localizado na pasta resources/views, e
     * seu nome deve ser informado no seguinte formato: <diretório>.<nome do arquivo sem .blade.php>     *
     * Por exemplo, se o parâmetro $viewName for "main.index", o método retornará a view gerada pelo arquivo
     * resources/views/main/index.blade.php
     *
     * Se a view especificada em $viewName não existir, a view error.404 será retornada.
     *
     * @param string $viewName O nome da view, no formato Blade
     * @param Response $response o objeto de resposta
     * @return Response
     */
    public function get(string $viewName, Response $response) : Response
    {
        $viewCachePath = 'viewCache'; //diretório de cache de views.
        if(!is_dir($viewCachePath)) { //se o diretório de cache não existir
            mkdir($viewCachePath); //cria o diretório
        }
        $viewPath = 'resources' . DIRECTORY_SEPARATOR . 'views'; //definindo o diretório das views.
        $blade = new Blade($viewPath, $viewCachePath); //criando o objeto da template engine Blade

        $viewFilename = $viewPath . DIRECTORY_SEPARATOR .
            str_replace('.',DIRECTORY_SEPARATOR,$viewName) .
            '.blade.php'; //preparando o nome do arquivo da view...

        if(file_exists($viewFilename)) { //...para verificar aqui se ele existe.
            $viewContent = $blade->render($viewName); //renderizando o conteúdo da view.
            $response->getBody()->write($viewContent);
            return  $response;//e escrevendo na resposta;
        } //se o arquivo não existir
        $viewContent = $blade->render('error.404'); //renderiza a view de erro
        $response->getBody()->write($viewContent); //e escreve na resposta
        return $response->withStatus(404); //setando o estado para 404.

    }

}