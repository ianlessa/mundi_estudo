<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 22:01
 */

namespace MundiEstudo\core\persister;

use MundiEstudo\core\config\ConfigInterface;
use MundiEstudo\traits\Persistable\PersistableInterface;

/**
 * Interface para classes que persistem objetos em bases de dados.
 *
 * @author Ian Lessa
 * @package MundiEstudo\core\persister
 */
interface PersisterInterface
{



    /**
     * Libera os recuros alocados pelo objeto;
     * Fecha a conexão com o banco de dados
     *
     *
     * @author Ian Lessa
     */
    public function __destruct();

    /**
     * Procura um objeto em uma base de dados definida pelo construtor da classe.
     * O local a ser procurado deverá ser definido pela constante type do objeto passado
     * como parâmetro, bem como seu identificador, definido por seu atributo
     * id.
     *
     * Caso o objeto procurado pelo id informado no $baseObject seja encontrado,
     * o objeto passado como parâmetro será carregado com os atributos do
     * elemento encontrado. Caso contrário, permanecerá intacto.
     *
     * @author Ian Lessa
     * @param PersisterSourceObjectInterface $baseObject O objeto a ser procurado
     * @return bool True se o objeto foi encontrado, false se não.
     */
    public function find(PersisterSourceObjectInterface &$baseObject) : bool;

    /**
     * Cria um objeto, definido por $baseObject.
     *
     * O objeto é persistido e seu atributo id é setado para o id correspondente
     * ao registro inserido na base de dados.
     *
     * Como o método PeristeInterface::find(), também define o local de salvamento do
     * objeto através de sua constante type.
     *
     * @author Ian Lessa
     * @param PersisterSourceObjectInterface $baseObject O objeto a ser criado
     * @return bool True se o objeto foi criado, false se houve erro.
     * @see PersistableInterface::find
     */
    public function create(PersisterSourceObjectInterface &$baseObject) : bool;

    /**
     * Atualiza um objeto, definido por $baseObject.
     *
     * O atributo id do objeto passado como parâmetro é usado para buscar o objeto a ser
     * atualizado.
     *
     * Como o método PeristeInterface::find(), também define o local de salvamento do
     * objeto através de sua constante type.
     *
     * @author Ian Lessa
     * @param PersisterSourceObjectInterface $baseObject O objeto a ser atualizado
     * @return bool True se o objeto foi atualizado, false se houve erro.
     * @see PersistableInterface::find
     */
    public function update(PersisterSourceObjectInterface &$baseObject) : bool;


    /**
     * Lista todos os registros de um determinado tipo.
     * O tipo de objeto retornado é pela  classe informada,
     * bem como o local de procura, definido por sua constante type.
     * A classe passada deverá implementar a interface PersisterSourceObjectInterface.
     *
     * @author Ian Lessa
     * @param string $class O nome da classe dos objetos a serem listados.
     * @return array Uma array de objetos, do tipo $class, encontrados ou uma array vazia
     *  caso não sejam encontrados registros.
     */
    public function list(string $class) : array;

    /**
     * Remove um objeto dos registros. O objeto cujo o id seja igual ao atributo
     * id de $baseObject será removido da base de dados.
     * Caso o objeto não exista, nada será feito.
     *
     * O local de pesquisa é definido pela constante type do objeto passado como
     * parâmetro.
     *
     * @author Ian Lessa
     * @param PersisterSourceObjectInterface $baseObject O objeto a ser removido
     * @return bool True em caso de sucesso, false em caso de erro.
     */
    public function delete(PersisterSourceObjectInterface &$baseObject) : bool;


    /**
     * Busca por objetos da classe $class que contenham o termo $term
     *
     *
     * @author Ian Lessa
     * @param string $class A classe do objeto a ser procurada
     * @param string $term O termo usado na busca
     * @return array Os resultados da busca.
     */
    public function search(string $class,string $term) : array;


}