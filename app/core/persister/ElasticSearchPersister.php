<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 21:44
 */

namespace MundiEstudo\core\persister;

use Elasticsearch\ClientBuilder;
use MundiEstudo\core\config\ConfigInterface;

class ElasticSearchPersister extends Persister
{

    /**
     * Construtor
     *
     * @author Ian Lessa
     * @see Persister::__construct();
     */
    public function __construct(ConfigInterface $connectionConfig)
    {
        parent::__construct($connectionConfig, 'ElasticSearch');
    }

    /**
     *
     * @author Ian Lessa
     * @see PersisterInterface::find()
     */
    public function find(PersisterSourceObjectInterface &$baseObject) : bool {

        //separando tipo do objeto (index/type) em index e type.
        $params = $this->getIndexAndType($baseObject->getType());

        //verificando se o índice existe no ElasticSearch
        $indexParams = array(
            'index' => $params['index']
        );
        if(!$this->connection->indices()->exists($indexParams)) {
            return false; //não existe index..
        }


        //Preparando parâmetros para busca no ElasticSearch
        $params['body'] = array (
            'query' => array(
                'ids' => array (
                    "type" => $params['type'],
                    "values" => array($baseObject->getId())
                )
            )
        );
        $response = $this->connection->search($params); //executando a busca
        if($response['hits']['total'] === 0) { //se não forem encontrados resultados
            return false;
        }
        $objectArray = $response['hits']['hits'][0]['_source']; //obtendo a array com o objeto encontrado.
        $objectArray['id'] = $response['hits']['hits'][0]['_id']; //setando o id do objeto encontrado para importação
        $baseObject->import(json_encode($objectArray)); //setando o objeto parâmetro com os dados encontrados.
        return true;
    }

    /**
     *
     * @author Ian Lessa
     * @see PersisterInterface::update()
     */
    public function update(PersisterSourceObjectInterface &$baseObject) : bool {
        //separando tipo do objeto (index/type) em index e type.
        $params = $this->getIndexAndType($baseObject->getType());
        $objectArray = $baseObject->export(); //exportando os dados do objeto para JSON
        $objectArray = json_decode($objectArray); //o JSON exportado para stdClass
        $objectArray = (array)$objectArray; //convertendo o objeto stdClass para array;

        //clonando $baseObject
        $objectClass = get_class($baseObject); //recuperando a classe do objeto parâmetro
        $tempObject = new $objectClass(); //criando um objeto temporário igual a objeto parâmetro.
        $tempObject->import($baseObject->export()); //clonando o objeto

        //verifica se o objeto existe
        if($this->find($tempObject)) {
            //existe: atualizar
            //preparando os parâmetros para atualização
            $params['id'] = $baseObject->getId();
            $params['body'] = array(
                'doc' => $objectArray
            );
            $this->connection->update($params); //atualizando no ElasticSearch.
            return true;
        }
        return false; //tudo ok.
    }

    /**
     *
     * @author Ian Lessa
     * @see PersisterInterface::create()
     */
    public function create(PersisterSourceObjectInterface &$baseObject) : bool {
        //separando tipo do objeto (index/type) em index e type.
        $params = $this->getIndexAndType($baseObject->getType());
        $objectArray = $baseObject->export(); //exportando os dados do objeto para JSON
        $objectArray = json_decode($objectArray); //o JSON exportado para stdClass
        $objectArray = (array)$objectArray; //convertendo o objeto stdClass para array;

        $params['body'] = $objectArray; //preparando os parâmetros para criação
        $response = $this->connection->index($params); //criando objeto no ElasticSearch
        $baseObject->setId($response['_id']); //setando o ide de $baseObject com o id de inserção no ElasticSearch
        return true; //tudo ok.
    }


    /**
    *
    * @author Ian Lessa
    * @see PersisterInterface::list()
    */
    public function list(string $class) : array {
        //verifica se a classe informada existe
        if(!class_exists($class)) {
            return array(); //não existe classe
        }
        //verifica se a classe informada implementa a interface necessária.
        if(!isset(class_implements($class)
            ['MundiEstudo\core\persister\PersisterSourceObjectInterface'])) {
            return array(); //não implementa interface
        }
        //separando tipo do objeto (index/type) em index e type.
        $params = $this->getIndexAndType($class::type);
        //preparando parâmetros para operação no elasticSearch.
        $params['body'] = array (
            'query' => array(
                'match_all' => array( /*para buscar todos os elementos*/
                    "boost" => 1.0 /*sem este parâmetro, a query não funciona. Parece que é um bug da biblioteca */
                )
            )
        );
        //verificando se o índice existe no ElasticSearch
        $indexParams = array(
            'index' => $params['index']
        );
        if(!$this->connection->indices()->exists($indexParams)) {
            return array(); //não existe index..
        }
        //contando os resultados, para q sejam retornados todos na busca.
        $response = $this->connection->count($params); //contando resultados.
        $params['body']['size'] = $response['count']; //para retornar todos os resultados
        //busca no elasticSearch pelos elementos do indice
        $response = $this->connection->search($params);

        if($response['hits']['total'] === 0) {
            return array(); //se não forem encontrados resultados
        }

        $hits = $response['hits']['hits']; //pegandos os objetos encontrados
        $results = array(); //para armazenar os objetos que serão criados a partir dos resuldados.
        foreach($hits as $hit) {
            $object = (object)$hit['_source']; //armazenando os resultados em array temporária.
            $object->id = $hit['_id']; //setando o id do resultado
            $json = json_encode($object); //codificando a array em JSON
            $object = new $class(); //criando objeto temporário
            $object->import($json); //setando os dados do objeto temporário com o JSON da array temporária
            $results[] = $object; //adicionando objeto temporário à array de resultados.
        }
        return $results;
    }

    /**
     *
     * @author Ian Lessa
     * @see PersisterInterface::delete()
     */
    public function delete(PersisterSourceObjectInterface &$baseObject) : bool {
        //separando tipo do objeto (index/type) em index e type.
        $params = $this->getIndexAndType($baseObject->getType());
        $params['id'] = $baseObject->getId(); //setando o id do objeto para deleção

        $this->connection->delete($params); //deletando

        return true; //tudo ok
    }

    /**
     *
     * @author Ian Lessa
     * @see Persister::makeConnection()
     */
    protected function makeConnection()
    {
        //definindo parâmetros padrão
        $host = 'localhost';
        $port = '9200';

        //pegando configurações
        $this->config->get($this->configSection,'host',$host);
        $this->config->get($this->configSection,'port',$port);

        //criando cliente do ElasticSearch e armazenando.
        $this->connection = ClientBuilder::create()
            ->setHosts(
              array(
                  "$host:$port"
              )
            )
            ->build();
    }

    /**
     * Separa index e type do tipo informado, para compatibiliade com o ElasticSearch.
     * O parâmetro type deverá ser informado no formato index/type.
     *
     * @author Ian Lessa
     * @param string $type
     * @return array
     */
    protected function getIndexAndType(string $type) : array {
        $type = explode('/',$type);
        $index = $type[0];
        $type = $type[1];
        return array(
            'index' => $index,
            'type' => $type,
        );

    }

    /**
     * Fecha a conexão com a base dados.
     *
     * @author Ian Lessa
     * @see Persister::closeConnection()
     */
    protected function closeConnection()
    {
        unset($this->connection);
    }

    /**
     * Busca por objetos da classe $class que contenham o termo $term
     *
     *
     * @author Ian Lessa
     * @see PersisterInterface::search()
     */
    public function search(string $class, string $term): array
    {
        //verifica se a classe informada existe
        if(!class_exists($class)) {
            return array(); //não existe classe
        }
        //verifica se a classe informada implementa a interface necessária.
        if(!isset(class_implements($class)
            ['MundiEstudo\core\persister\PersisterSourceObjectInterface'])) {
            return array(); //não implementa interface
        }
        //separando tipo do objeto (index/type) em index e type.
        $params = $this->getIndexAndType($class::type);
        //preparando parâmetros para operação no elasticSearch.
        $params['body'] = array (
            'query' => array(
                'match' => array( /*para buscar os elementos*/
                    "_all" => $term
                )
            )
        );
        //verificando se o índice existe no ElasticSearch
        $indexParams = array(
            'index' => $params['index']
        );
        if(!$this->connection->indices()->exists($indexParams)) {
            return array(); //não existe index..
        }
        //contando os resultados, para q sejam retornados todos na busca.
        $response = $this->connection->count($params); //contando resultados.
        $params['body']['size'] = $response['count']; //para retornar todos os resultados
        //busca no elasticSearch pelos elementos do indice
        $response = $this->connection->search($params);

        if($response['hits']['total'] === 0) {
            return array(); //se não forem encontrados resultados
        }

        $hits = $response['hits']['hits']; //pegandos os objetos encontrados
        $results = array(); //para armazenar os objetos que serão criados a partir dos resuldados.
        foreach($hits as $hit) {
            $object = (object)$hit['_source']; //armazenando os resultados em array temporária.
            $object->id = $hit['_id']; //setando o id do resultado
            $json = json_encode($object); //codificando a array em JSON
            $object = new $class(); //criando objeto temporário
            $object->import($json); //setando os dados do objeto temporário com o JSON da array temporária
            $results[] = $object; //adicionando objeto temporário à array de resultados.
        }
        return $results;

    }
}