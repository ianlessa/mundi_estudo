<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 21:52
 */

namespace MundiEstudo\core\persister;

use MundiEstudo\traits\FrontendTranslator\FrontendTranslatorInterface;
use MundiEstudo\traits\Persistable\PersistableInterface;

/**
 * Combina as interfaces PersistableInterface e FrontendTranslatorInterface
 *
 * @author Ian Lessa
 * @package MundiEstudo\core\persister
 * @see PersistableInterface
 * @see FrontendTranslatorInterface
 */
interface PersisterSourceObjectInterface extends
    PersistableInterface, FrontendTranslatorInterface
{

}