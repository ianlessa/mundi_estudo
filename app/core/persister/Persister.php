<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 22:02
 */

namespace MundiEstudo\core\persister;

use MundiEstudo\core\config\ConfigInterface;

/**
 * Classe abstrata das classes de persistência de objetos
 *
 * @author Ian Lessa
 * @package MundiEstudo\core\persister
 */
abstract class Persister implements PersisterInterface
{
    protected $config; //configurações de conexão
    protected $configSection; //sessão de configuração com os parâmetros de conexão
    protected $connection; //conexão para persistência



    /**
     * Construtor da interface. Deverá ser passado um objeto de configuração,
     * já carregado, contendo as configurações de conexão com uma base de dados.
     *
     * @author Ian Lessa
     * @param ConfigInterface $connectionConfig Objeto com informações de conexão
     * @param string $configSection Sessão de configuração com parâmetros de conexão.
     */
    public function __construct(ConfigInterface $connectionConfig, string $configSection)
    {
        $this->config = $connectionConfig;
        $this->configSection = $configSection;
        $this->makeConnection(); //criando a conexão para persistência.
    }

    /**
     * Destrutor
     *
     * @author Ian Lessa
     * @see PersisterInterface::__destruct();
     */
    public function __destruct()
    {
        $this->closeConnection(); //fecha a conexão criada.
    }


    /**
     * Cria o objeto de conexão para a persistência.
     *
     * @author Ian Lessa
     */
    abstract protected function makeConnection();

    /**
     * Fecha a conexão com a base dados.
     *
     * @author Ian Lessa
     */
    abstract protected function closeConnection();

}