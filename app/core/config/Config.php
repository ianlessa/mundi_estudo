<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 21:13
 */

namespace MundiEstudo\core\config;

/**
 * Classe abstrata que implementa métodos comuns a todas as implementações de
 * ConfigInterface.
 *
 * @author Ian Lessa
 * @package MundiEstudo\core\config
 */
abstract class Config implements ConfigInterface
{
    protected $config; //para armazenar as configurações carregadas.

    /**
     * Implementação do método ConfigInterface::get();
     *
     * @author Ian Lessa
     * @see ConfigInterface::get()
     */
    public function get(string $section, string $key, string &$out) : bool {
        if(!isset($this->config[$section])){
            return false; //$section não existe na configuração carregada.
        }
        if(!isset($this->config[$section][$key])){
            return false; //$key não existe na configuração carregada.
        }
        $out = $this->config[$section][$key]; //setando parâmetro de saída.
        return true; //configuração localizada.
    }

    /**
     * Implementação do método ConfigInterface::getAll();
     *
     * @author Ian Lessa
     * @see ConfigInterface::getAll()
     */
    public function getAll() : array {
        return $this->config;
    }



}