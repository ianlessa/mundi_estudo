<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 21:12
 */

namespace MundiEstudo\core\config;

/**
 * Interface para carregadores de configurações.
 *
 * @author Ian Lessa
 * @package MundiEstudo\core\config
 */
interface ConfigInterface
{
    /**
     *
     * Carrega configurações de uma fonte definida por $source, armazenando
     * as informações numa array interna.
     *
     * @author Ian Lessa
     * @param string $source A fonte da configuração
     * @return bool True se a configuração foi carregada ou False se houver erro.
     */
    public function load(string $source) : bool;

    /**
     * Retorna um valor de configuração através do parâmetro &$out.
     * Se $section e $key não existirem na array de configurações
     * carregada, o parâmetro $out não é alterado e o método retorna false.
     *
     * @author Ian Lessa
     * @param string $section Sessão da configuração
     * @param string $key Chave da configuração
     * @param string $out Parâmetro de saída para configuração
     * @return bool True se foi possível obter a configuraçção, false se não.
     */
    public function get(string $section, string $key, string &$out) : bool ;


    /**
     * Retorna todas as configurações carregadas e armazenadas no objeto.
     *
     * @author Ian Lessa
     * @return array A array de configurações carregadas
     */
    public function getAll() : array;
}