<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 21:13
 */

namespace MundiEstudo\core\config;


/**
 * Classe que implementa o carregamento de configuração a partir
 * de um arquivo .ini
 * @package MundiEstudo\core\config
 */
class IniConfig extends Config
{
    /**
     * Implementação do método ConfigInterface::Load().
     * Carrega configurações de um arquivo .ini, que deverá estar localizado em
     * app/config.
     *
     * @author Ian Lessa
     * @param string $source Nome do arquivo .ini a ser carregado, localizado em
     *  app/config.
     * @see ConfigInterface::load()
     */
    public function load(string $source) : bool
    {
        $filename = "app/config/$source"; //definindo caminho para o arquivo.
        if(file_exists($filename)) { //arquivo existe?
            //carregando as configurações do arquivo.
            $this->config = parse_ini_file($filename,true);
            return true; //carregamento ok.
        }

        return false; //não conseguiu carregar.
    }
}