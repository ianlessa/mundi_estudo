<?php
/**
 * Created by PhpStorm.
 * User: tr610600
 * Date: 26/05/2017
 * Time: 11:36
 */

namespace MundiEstudo\core\objectFactories;
use MundiEstudo\core\config\ConfigInterface;
use MundiEstudo\core\persister\PersisterSourceObjectInterface;

/**
 * Classe fábrica dos itens persistíveis acessiveis pela API RESTFul.
 *
 * @package MundiEstudo\core\objectFactories
 * @author Ian LEssa
 */
class PersistableObjectFactory
{
    private $factoryConfig; //configurações da fábrica


    /**
     * Construtor.
     *
     * @author Ian Lessa
     * @param ConfigInterface $factoryConfig
     */
    public function __construct(ConfigInterface $factoryConfig){
        $this->factoryConfig = $factoryConfig;
    }

    /**
     * Cria o item do tipo informado por $itemType e o retorna.
     * O tipo de item deve ser uma sessão na configuraçao armazenada, e a
     * classe do item será a chave "class" desta sessão.
     *
     * @author Ian Lessa
     * @param string $itemType O tipo de item que será criado.
     * @return PersisterSourceObjectInterface|null
     */
    public function create(string $itemType) : ?PersisterSourceObjectInterface {
        $class = ''; //armazenará a classe
        //Tenta pegar, na sessão de configuração definida por $itemType, a classe do item a ser criada
        if($this->factoryConfig->get($itemType,'class',$class) === false ||
            !class_exists($class)) {
            //Se não for possível pegar a sessão de configuração ou a classe não existir, retorna null
            return null;
        }

        return new $class(); //se a classe existir, cria o novo objeto desta classe e o retorna.
    }

    /**
     * De acordo com o tipo do item passado, seguindo os mesmos critérios do método create,
     * retorna a classe deste tipo de item.
     *
     * @author Ian Lessa
     * @param string $itemType O Tipo do item cuja classe é desejada.
     * @return null|string
     * @see PersistableObjectFactory::create();
     */
    public function getClass(string $itemType) : ?string {
        $class = ''; //armazenará a classse
        //Tenta pegar, na sessão de configuração definida por $itemType, a classe do item a ser criada
        if($this->factoryConfig->get($itemType,'class',$class) === false ||
            !class_exists($class)) {
            //Se não for possível pegar a sessão de configuração ou a classe não existir, retorna null
            return null;
        }
        return $class; //se a classe existe, retorna o seu nome.
    }

    /**
     * Retorna a lista de classes que podem ser criadas por esta fábrica.
     *
     * @author Ian Lessa
     * @return array A lista de nomes de classes que podem ser criadas por esta fábrica
     */
    public function getProductClasses() : array {

        $configs = $this->factoryConfig->getAll(); //pegando todas as configurações da fábrica

        $productClasses = array(); //a lista de classes
        foreach($configs as $config) { //para cada sessão de configuração
            //se a chave existe na sessão de configuração e a classe existe.
            if(isset($config['class']) && class_exists($config['class'])) {
                $productClasses[] = $config['class']; //armazena a classe que pode ser criada.
            }
        }
        return $productClasses; //retorna a lista de classes
    }

}