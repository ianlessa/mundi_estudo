<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 25/05/2017
 * Time: 05:48
 */

namespace MundiEstudo\controllers;

use MundiEstudo\core\objectFactories\PersistableObjectFactory;
use MundiEstudo\core\persister\PersisterInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use MundiEstudo\core\config\IniConfig;
use MundiEstudo\core\persister\ElasticSearchPersister;

/**
 * Classe abstrata de Controller. Esta carrega as configurações de acesso ao BD e direciona o tratamento das
 * requisições HTTP para os métodos correspondentes, de acordo com o método da requisição.
 *
 * É componente importante na API RESTFul.
 *
 * @author Ian Lessa
 *
 */
abstract class Controller
{
    /** @var PersisterInterface O persistidor que será usado pela API  */
    protected $persister;
    /** @var PersistableObjectFactory A classe fábrica de objetos que serão tratados pela API */
    protected $objectFactory;
    /** @var  Request o objeto de Requisição, gerado pelo Slim */
    protected $request;
    /** @var  Response o objeto de resposta, gerado pelo Slim*/
    protected $response;
    /** @var  array a array contendo os parâmetros da requisição, gerada pelo Slim */
    protected $args;

    /**
     * Construtor.
     * Carrega os arquivos de configuração e instancia o a fábrica e o persistidor de objetos.
     *
     * @author Ian Lessa
     */
    public function __construct()
    {
        //carregando configurações do banco de dados
        $persisterConfig = new IniConfig();
        $persisterConfig->load('database.ini');

        //carregando configurações da fábrica de objetos
        $objectFactoryConfig = new IniConfig();
        $objectFactoryConfig->load('PersistableObjectFactoryProducts.ini');

        $this->persister = new ElasticSearchPersister($persisterConfig); //instancia o persistidor ElasticSearch
        $this->objectFactory = new PersistableObjectFactory($objectFactoryConfig); //instancia a fábrica de objetos.
    }

    /**
     *
     * Este método é invocado pelo Slim quando a rota correta, definida em app/routes.php, é requerida.
     *
     * @author Ian Lessa
     * @param Request $request o objeto de Requisição, gerado pelo Slim
     * @param Response $response o objeto de Resposta, gerado pelo Slim
     * @param array $args a array contendo os parâmetros da requisição, gerada pelo Slim
     * @return Response a Resposta processada.
     */
    public function __invoke(Request $request, Response $response, array $args = []) {
        //verificando se o parametro id existe
        $itemId = isset($args['id']) ? $args['id'] : null;
        //$setando os atributos da classe
        $this->response = $response;
        $this->request = $request;
        $this->args = $args;

        if($itemId === null) { //se o parametro id não foi passado na url
            switch ($request->getMethod()) {
                //de acordo com o método de requisição, executa o método específico da classe
                case 'GET': return $this->index();
                case 'POST': return $this->create();
                case 'PATCH': return $this->update();
                case 'DELETE': return $this->delete();
            }
        }
        else { //se o parametro id foi passado na url
            switch ($request->getMethod()) {
                //de acordo com o método de requisição, executa o método específico da classe
                case 'GET': return $this->search();
                case 'DELETE': return $this->delete();
            }
        }
    }

    /**
     * Método implementado para reponder à omissão do método correspondente nas classes filhas.
     *
     * @author Ian Lessa
     * @return Response
     */
    protected function index() : Response {
        return $this->response->withStatus(404);
    }
    /**
     * Método implementado para reponder à omissão do método correspondente nas classes filhas.
     *
     * @author Ian Lessa
     * @return Response
     */
    protected function create() : Response {
        return $this->response->withStatus(404);
    }
    /**
     * Método implementado para reponder à omissão do método correspondente nas classes filhas.
     *
     * @author Ian Lessa
     * @return Response
     */
    protected function update() : Response {
        return $this->response->withStatus(404);
    }
    /**
     * Método implementado para reponder à omissão do método correspondente nas classes filhas.
     *
     * @author Ian Lessa
     * @return Response
     */
    protected function delete() : Response {
        return $this->response->withStatus(404);
    }
    /**
     * Método implementado para reponder à omissão do método correspondente nas classes filhas.
     *
     * @author Ian Lessa
     * @return Response
     */
    protected function search() : Response {
        return $this->response->withStatus(404);
    }

}