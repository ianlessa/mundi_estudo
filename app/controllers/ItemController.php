<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 24/05/2017
 * Time: 04:17
 */

namespace MundiEstudo\controllers;
use MundiEstudo\core\config\IniConfig;
use MundiEstudo\core\persister\ElasticSearchPersister;
use MundiEstudo\model\Item\BookItem;
use MundiEstudo\model\Item\CDItem;
use MundiEstudo\model\Item\DVDItem;
use MundiEstudo\traits\FrontendTranslator\FrontendTranslatorInterface;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Classe da API RESTFul que responde às requisições de
 * pesquisa e listagem de itens da página inicial da aplicação
 *
 * @author Ian Lessa
 * @package MundiEstudo\controllers
 */
final class ItemController extends Controller
{

    /**
     * Obtem todos os livros, cds e dvds cadastrados e os retorna em um json.
     *
     * @author Ian Lessa
     * @return Response A lista de itens, em formato json.
     */
    protected function index() : Response {

        //carregando todos os items emprestáveis;
        $allItems = array_merge(
            $this->persister->list(BookItem::class),
            $this->persister->list(CDItem::class),
            $this->persister->list(DVDItem::class)
        );
        $results = array();

        /** convertendo todos os itens encontrados para array, para que possam ser enviados ao frontend
         * @var FrontendTranslatorInterface $item */
        foreach($allItems as $item) {
            $array = $item->export();
            $array = json_decode($array);
            $results[] = $array;
        }
        return $this->response->withJson($results); //retornando os resultados como json.
    }

    /**
     * Realiza a busca por um termo nas listas de pessoas, tags, livros, cds e dvds.
     *
     * @author Ian Lessa
     * @return Response Os resultados da busca, em formato json.
     */
    protected function search() : Response{

        $searchTerm =  $this->args["id"]; //separando o termo para busca
        $itemClasses = $this->objectFactory->getProductClasses(); //retornando a lista de classes instanciáveis

        $allResults = array(); //resultado geral da busca.
        foreach($itemClasses as $class) { //para cada uma das classes instanciáveis
            $allResults = array_merge(
                $allResults,
                $this->persister->search($class,$searchTerm) /*adicionando os resultados da busca ao resultado geral. */
            );
        }

        $results = array(); //para armazenar os resultados convertidos
        foreach ($allResults as $item) { //para cada resultado, converte para array e armazena
            $array = $item->export();
            $array = json_decode($array);
            $results[] = $array;
        }
        return $this->response->withJson($results); //retonando os resultados como json
    }


}