<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 25/05/2017
 * Time: 05:57
 */

namespace MundiEstudo\controllers;

use Psr\Http\Message\ResponseInterface as Response;

class WebserviceController extends Controller
{


    /**
     * Obtem todos os itens do tipos especificado por $itemType e os retorna em um json.
     * Responde às requisições do tipo GET, sem parâmetros além do tipo do item.
     *
     * @author Ian Lessa
     * @return Response A lista de itens, em formato json.
     */
    protected function index(): Response
    {
        $itemType = $this->args["itemType"]; //recuperando o tipo de item do Slim, que foi passado na url de requisição
        $results = array(); //para armazenar a lista de itens
        $class = $this->objectFactory->getClass($itemType); //recuperando a classe do tipo de item.
        if($class) { //se a classe for retornada
            $items = $this->persister->list($class); //recupera a lista de registros do tipo informado
            /** @var FrontendTranslatorInterface $item */
            foreach($items as $item) { //para cada registro encontrado, converte para array...
                $array = $item->export();
                $array = json_decode($array);
                $results[] = $array; //..e armazena nos resultados
            }
        }
        return $this->response->withJson($results); //enviando os resultados em formato json.
    }

    /**
     * Cria um item, do tipo defindo por $itemType, com os dados que foram recebidos
     *  via POST e o retorna, em formato JSON
     *
     * Responde às requisições do tipo POST, sem parâmetros além do tipo do item na url.
     *
     * @author Ian Lessa
     * @return Response O item criado, em formato json
     */
    protected function create(): Response
    {
        $itemType = $this->args["itemType"]; //recuperando o tipo do item, que o Slim pegou da url de requisição.
        $item = $this->objectFactory->create($itemType); //criando o item temporário
        if($item) { //se o item temporário pode ser criado
            $json = json_encode($this->request->getParsedBody()); //prepara um json com os dados do POST...
            $item->import($json); //...e usa este json para iniciar o item temporário
            if($this->persister->create($item)) { //se o objeto pode ser criado pelo persister
                $this->response->getBody()->write($item->export()); //responde com o JSON correspondente ao item...
                return $this->response->withStatus(200); //...e status HTTP 200
           }
        }
        return $this->response->withStatus(404); //se o item não pode ser criado, responde com 404.
    }

    /**
     * Atualiza um item, do tipo definido por $itemType, com as informações recebidas no corpo da requisição PATCH.
     *
     * Responde às requisições do tipo PATCH, sem parâmetros além do tipo do item na url.
     *
     * @author Ian Lessa
     * @return Response uma resposta com o código de erro 200 se o item foi atualizado ou 404, se houve erro.
     */
    protected function update(): Response
    {
        $itemType = $this->args["itemType"];  //recuperando o tipo do item, que o Slim pegou da url de requisição.
        $item = $this->objectFactory->create($itemType); //criando o item temporário
        if($item) { //se o item pode ser criado
            $json = json_encode($this->request->getParsedBody()); //pegando os dados do PATCH...
            $item->import($json); //...e os usando para inicializar o item temporário

            if($this->persister->update($item)) { //foi possível atualizar o objeto?
                return $this->response->withStatus(200); //retorna http status 200.
            }
        }
        return $this->response->withStatus(404); //objeto não pode ser atualizado, retorna http status 404.
    }

    /**
     * Apaga um item, do tipo definido por $itemType, e cujo id seja o informado na URL da requisição DELETE
     * Responde às requisições do tipo DELETE, cujo único parametro na URL além do tipo do item é
     *  o ID do item a ser deletado.
     *
     * @return Response  uma resposta com o código de erro 200 se o item foi apagado ou 404, se houve erro.
     */
    protected function delete() : Response {
        $itemType = $this->args["itemType"]; //pegando  tipo do item a ser deletado
        $itemId = $this->args["id"];  //pegando o id do item a ser deletado;
        $item = $this->objectFactory->create($itemType); //criando item temporário
        if($item) { //se o item pode ser deletado
            $item->setId($itemId); //ajusta o id no item temporário

            if($this->persister->delete($item)) { //objeto deletado?
                return $this->response->withStatus(200); //se sim, retorna status http 200
            }
        }
        return $this->response->withStatus(404); //tipo de objeto não existe, retorna 404.
    }
}