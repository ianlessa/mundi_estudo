<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 20/05/2017
 * Time: 23:06
 */

use MundiEstudo\core\view\ViewServer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Arquivo que define as rotas da aplicação.
 */

//Rota principal
$app->get('/',function(Request $request, Response $response) use ($app) {
    return (new ViewServer())->get('main.index',$response);
});

//Rota servidora de views
$app->get('/_view/{viewName}', function(Request $request, Response $response){
    $viewName = $request->getAttribute('viewName');
    return (new ViewServer())->get($viewName,$response);
});

//rotas de webservice RESTful
//para todso os items
$app->group('/item',function(){
    $this->map(['GET'],'','MundiEstudo\controllers\ItemController');
});
$app->group('/itemSearch/{id}',function(){
    $this->map(['GET'],'','MundiEstudo\controllers\ItemController');
});
//para tipos de items específicos
$app->group('/item/{itemType}[/{id}]',function(){
    $this->map(['GET','POST','PATCH','DELETE'],'','MundiEstudo\controllers\WebserviceController');
});
