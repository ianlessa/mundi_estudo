<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 19:08
 */

namespace MundiEstudo\traits\Persistable;

/**
 * Trait de objetos persistíveis. Implementa os métodos definidos na interface.
 *
 * @author Ian Lessa
 * @package MundiEstudo\traits\Persistable
 * @see PersistableInterface
 */
trait PersistableTrait
{
    /** @var string O índice do objeto persistível */
    protected $id;
    /** @var string o tipo do objeto persistível, que será usado para localização da tabela/indice da classe */
    protected $type;

    public function getId() : string {
        return $this->id;
    }
    public function setId(string $id) {
        $this->id = $id;
    }

    public function getType() : string {
        return $this->type;
    }
}