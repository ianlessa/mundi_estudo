<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 18:55
 */

namespace MundiEstudo\traits\Persistable;

/**
 * Interface para objetos persistíveis.
 * @author Ian Lessa
 * @package MundiEstudo\traits\Persistable
 */
interface PersistableInterface
{
    public function getId() : string;
    public function setId(string $id);

    public function getType() : string;

}