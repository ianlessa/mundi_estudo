<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 19:09
 */

namespace MundiEstudo\traits\FrontendTranslator;

/**
 * Trait com a implementação dos métodos do Tradutor de Objetos para JSON.
 *
 * @author Ian Lessa
 * @package MundiEstudo\traits\FrontendTranslator
 * @see FrontendTranslatorInterface
 */
trait FrontendTranslatorTrait
{
    /**
     * @author ian Lessa
     * @return string
     * @see FrontendTranslatorInterface::export()
     */
    public function export() : string {

        //instanciando uma classe Reflection, que pegara os atributos do objeto.
        $reflection = new \ReflectionClass($this);
        $properties = $reflection->getProperties(); //pegando os atributos do objeto

        $exportObject = new \stdClass(); //criando objeto temporário para exportação
        foreach($properties as $property) { //para cada propriedade encontrada
            $propertyName = $property->name;

            $propertyGetter = "get" . ucfirst($propertyName); //definindo nome do Getter da propriedade
            if(!method_exists($this,$propertyGetter)) { //se o método não existe
                continue; //continua o loop e não seta a propriedade.
            }
            $propertyValue = $this->$propertyGetter(); //invocando o Getter da propriedade.
            $exportObject->$propertyName = $propertyValue; //setando a propriedade no objeto stdClass.
        }

        return json_encode($exportObject); //convertendo o objeto temporário gerado para JSON e retornando.
    }

    /**
     * @author Ian Lessa
     * @param string $json
     * @see FrontendTranslatorInterface::import()
     */
    public function import(string $json) {
        $importObject = json_decode($json); //criando um objeto stdClass.
        $properties = get_object_vars($importObject); //convertendo o objeto para array associativa.
        foreach($properties as $propertyName => $value) {  // para cada propriedade encontrada
            $propertySetter = "set" . ucfirst($propertyName); //definindo Setter padrão
            if(method_exists($this,$propertySetter)) { //se o setter existir
                $this->$propertySetter($value); //o invoca.
            }
            //se o setter padrão não existir, a propriedade é ignorada.
        }
    }
}