<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 19:00
 */

namespace MundiEstudo\traits\FrontendTranslator;

/**
 * Interface do Tradutor de Objetos para JSON, que define os métodos para
 * importação e exportação de um objeto via JSON
 *
 * @package MundiEstudo\traits\FrontendTranslator
 * @author Ian Lessa
 */
interface FrontendTranslatorInterface
{
    /**
     * Exporta um objeto para uma string Json
     * Somente serão exportados os atributos que possuirem um método Getter padrão cammelCase definido
     *
     * @return string A string gerada a partir do objeto.
     */
    public function export() : string;
    /**
     * Define os atributos do objeto a partir de uma string json.
     * Somente serão importados os atributos que possuirem um método Setter padrão cammelCase definido
     *
     * @param string $json O JSON que será usado na importação.
     */
    public function import(string $json);
}