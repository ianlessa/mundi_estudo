<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 18:46
 */

namespace MundiEstudo\model\Tag;

use MundiEstudo\core\persister\PersisterSourceObjectInterface;
use MundiEstudo\traits\FrontendTranslator\FrontendTranslatorTrait;
use MundiEstudo\traits\Persistable\PersistableTrait;


/**
 * Classe que implementa a Interface de Tag, e a interface PersisterSourceObjectInterface, que torna
 * persistíveis os objetos desta classe.
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Tag
 * @see PersistableTrait
 * @see FrontendTranslatorTrait
 */
class Tag implements TagInterface, PersisterSourceObjectInterface
{
    use FrontendTranslatorTrait; //contém métodos para converão para JSON e inicialização via JSON
    use PersistableTrait; //contém atributos, getters e setters que serão usados para persistência do objeto

    /** @var string Nome da Tag */
    protected $name;

    const type = 'tags/tag'; //constante do tipo deste item.


    /**
     * Construtor.
     * Apenas inicializa os atributos.
     *
     * @author Ian Lessa
     */
    public function __construct()
    {
        $this->name = '';
        $this->id = ''; //PersistableTrait
        $this->type = static::type; //PersistableTrait
    }

    public function getName() : string {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name = $name;
    }


}