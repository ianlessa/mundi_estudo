<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 21:31
 */

namespace MundiEstudo\model\Tag;

/**
 * Interface de Tag
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Tag
 */
interface TagInterface
{
    public function getName() : string;
    public function setName(string $name);
}