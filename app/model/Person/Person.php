<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 20:25
 */

namespace MundiEstudo\model\Person;


use MundiEstudo\core\persister\PersisterSourceObjectInterface;
use MundiEstudo\traits\FrontendTranslator\FrontendTranslatorTrait;
use MundiEstudo\traits\Persistable\PersistableTrait;

/**
 * Classe que implementa a Interface de Pessoa, e a interface PersisterSourceObjectInterface, que torna
 * persistíveis os objetos desta classe.
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Person
 * @see PersistableTrait
 * @see FrontendTranslatorTrait
 */
class Person implements PersonInterface, PersisterSourceObjectInterface
{

    use FrontendTranslatorTrait; //contém métodos para converão para JSON e inicialização via JSON
    use PersistableTrait; //contém atributos, getters e setters que serão usados para persistência do objeto

    const type='persons/person';  //constante do tipo deste item.

    /** @var string Nome da Pessoa */
    protected $name;
    /** @var string Contato da Pessoa */
    protected $contact;

    /**
     * Construtor.
     * Apenas inicializa os atributos.
     *
     * @author Ian Lessa
     */
    public function __construct()
    {
        $this->name = '';
        $this->contact = '';
        $this->id = ''; //PersistableTrait
        $this->type = static::type; //PersistableTrait
    }

    public function getName() : string
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function getContact(): string
    {
        return $this->contact;
    }



    public function setContact(string $contact)
    {
        $this->contact = $contact;
    }


}