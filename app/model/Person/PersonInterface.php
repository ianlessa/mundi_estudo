<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 20:24
 */

namespace MundiEstudo\model\Person;

/**
 * Interface de Pessoa
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Person
 */
interface PersonInterface
{
    public function getName() : string;
    public function setName(string $name);

    public function getContact(): string;
    public function setContact(string $contact);
}