<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 18:35
 */

namespace MundiEstudo\model\Item;

/**
 * Trait que implementa os getters e setters comums a todos os items emprestáveis.
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Item
 */
trait ItemTrait
{
    /** @var string O nome do item */
    protected $name; //
    /**
     * @var array Armazena os ids das Tags associadas ao item.
     * @see Tag;
     */
    protected $tags;
    /**
     * @var string O id da pessoa que está com o item.
     * @see Person
     */
    protected $borrowerId;

    /**
     * @var float A avaliação deste item.
     */
    protected $rating;

    public function getName() : string {
        return $this->name;
    }
    public function setName(string $name) {
        $this->name = $name;
    }

    public function getTags() : array {
        return $this->tags;
    }
    public function setTags(array $tags) {
        $this->tags = $tags;
    }

    public function getBorrowerId() : string
    {
        return $this->borrowerId;
    }

    public function setBorrowerId(string $borrowerId)
    {
        $this->borrowerId = $borrowerId;
    }


    public function getRating(): float
    {
        return $this->rating;
    }

    public function setRating(float $rating)
    {
        $this->rating = $rating;
    }

}