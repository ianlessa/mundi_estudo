<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 18:32
 */

namespace MundiEstudo\model\Item;

/**
 * Classe de Item CD
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Item
 */
class CDItem extends Item
{
    const type = 'item/cd'; //constante do tipo deste item.

    /**
     * @var array Lista de Faixas do cd. Array de Strings
     */
    private $tracks;
    /**
     * @var string Nome do artista/banda
     */
    private $artist;

    public function getTracks(): array
    {
        return $this->tracks ? $this->tracks : array();
    }

    public function setTracks(array $tracks)
    {
        $this->tracks = $tracks;
    }

    public function getArtist(): string
    {
        return $this->artist ? $this->artist : '';
    }

    public function setArtist(string $artist)
    {
        $this->artist = $artist;
    }
}