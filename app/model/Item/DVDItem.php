<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 18:33
 */

namespace MundiEstudo\model\Item;

/**
 * Classe de item DVD
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Item
 */
class DVDItem extends Item
{
    const type = 'item/dvd'; //constante do tipo deste item.

    /**
     * @var string diretor do DVD.
     */
    private $director;

    /**
     * @var string Sinopse do DVD.
     */
    private $synopsis;


    public function getDirector(): string
    {
        return $this->director ? $this->director : '';
    }
    public function setDirector(string $director)
    {
        $this->director = $director;
    }


    public function getSynopsis(): string
    {
        return $this->synopsis ? $this->synopsis : '';
    }
    public function setSynopsis(string $synopsis)
    {
        $this->synopsis = $synopsis;
    }
}