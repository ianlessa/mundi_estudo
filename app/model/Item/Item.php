<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 18:42
 */

namespace MundiEstudo\model\Item;

use MundiEstudo\core\persister\PersisterSourceObjectInterface;
use MundiEstudo\traits\FrontendTranslator\FrontendTranslatorTrait;
use MundiEstudo\traits\Persistable\PersistableTrait;

/**
 * Classe abstrata de item, que concentra os comportamentos comuns a todos os items, como poderem ser persistidos,
 * convertidos para ou inicializados por JSON.
 *
 * Também implementa a interface PersisterSourceObjectInterface, que torna persistíveis os objetos filhos desta classe.
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Item
 * @see ItemTrait
 * @see PersistableTrait
 * @see FrontendTranslatorTrait
 */
abstract class Item implements ItemInterface, PersisterSourceObjectInterface
{
    use ItemTrait; //contém implementações dos getters e setters definidos na interface, bem como seus os atributos
    use PersistableTrait; //contém atributos, getters e setters que serão usados para persistência do objeto
    use FrontendTranslatorTrait; //contém métodos para converão para JSON e inicialização via JSON

    /**
     * Construtor.
     * Apenas inicializa os atributos. Estes são encontrados nos traits correspondentes.
     *
     * @author Ian Lessa
     */
    public function __construct() {
        $this->name = ''; //ItemTrait:
        $this->tags = array();  //ItemTrait:
        $this->borrowerId = ''; //ItemTrait
        $this->id = ''; //PersistableTrait
        $this->type = static::type; //PersistableTrait
        $this->rating = 0;
    }

}