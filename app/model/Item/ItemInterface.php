<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 18:31
 */

namespace MundiEstudo\model\Item;

/**
 * Interface para itens que podem ser emprestados.
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Item
 */

interface ItemInterface
{
    public function getName() : string;
    public function setName(string $name);

    public function getTags() : array;
    public function setTags(array $tags);

    public function getBorrowerId() : string;
    public function setBorrowerId(string $borrowerId);

    public function getRating(): float;
    public function setRating(float $rating);

}