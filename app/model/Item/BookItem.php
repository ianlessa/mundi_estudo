<?php
/**
 * Created by PhpStorm.
 * User: Ian
 * Date: 21/05/2017
 * Time: 18:31
 */

namespace MundiEstudo\model\Item;

/**
 * Classe de item Livro
 *
 * @author Ian Lessa
 * @package MundiEstudo\model\Item
 *
 */
class BookItem extends Item
{
    const type = 'item/book'; //constante do tipo deste item.

    /**
     * @var string Escritor do livro.
     */
    private $writer;

    /**
     * @var string Sinopse do livro.
     */
    private $synopsis;

    public function getWriter(): string
    {
        return $this->writer ? $this->writer : '';
    }
    public function setWriter(string $writer)
    {
        $this->writer = $writer;
    }


    public function getSynopsis(): string
    {
        return $this->synopsis ? $this->synopsis : '';
    }
    public function setSynopsis(string $synopsis)
    {
        $this->synopsis = $synopsis;
    }
}